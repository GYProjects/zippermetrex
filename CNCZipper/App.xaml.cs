﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CNCLib;
using System.Diagnostics;

namespace CNCZipper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Process proc = Process.GetCurrentProcess();
            int count = Process.GetProcesses().Where(p =>
                p.ProcessName == proc.ProcessName).Count();

            if (count > 1)
            {
                Current.Shutdown();
            }

            Logger.Write("App Started");

            var config = (NameValueCollection)ConfigurationManager.GetSection("settings");

            var c1 = ConfigurationManager.ConnectionStrings["DbCNC"];
            var c2 = ConfigurationManager.ConnectionStrings["DbClient"];

            DbBase db1 = new DbBase(c1.ProviderName, c1.ConnectionString);
            DbBase db2 = (c2 != null) ? new DbBase(c2.ProviderName, c2.ConnectionString) : null;

            if (!Global.Init(db1, db2, config))
            {
                Logger.Write("Global.Init Failed ...");
                Current.Shutdown();
            }

            MainWindow mainWindow = new MainWindow();
            mainWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            int screens = System.Windows.Forms.Screen.AllScreens.Length;

            System.Windows.Forms.Screen s2 = System.Windows.Forms.Screen.AllScreens[(screens > 1 ? 1 : 0)];

            mainWindow.Top = s2.WorkingArea.Top;
            mainWindow.Left = s2.WorkingArea.Left;
            mainWindow.Width = s2.WorkingArea.Width;
            mainWindow.Height = s2.WorkingArea.Height;

            mainWindow.Show();
        }
    }
}

﻿using System.Windows;

namespace CNCZipper
{
    /// <summary>
    /// Interaction logic for dlgConfirm.xaml
    /// </summary>
    public partial class dlgConfirm : Window
    {
        public dlgConfirm(Window parent, string question)
        {
            InitializeComponent();
            lblQuestion.Content = question;
            this.Owner = parent;

            this.Top = Owner.Top + 20;// + 
            this.Left = Owner.Left + Owner.Width / 2 - this.Width / 2;
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Owner.Effect = new System.Windows.Media.Effects.BlurEffect();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Owner.Effect = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using CNCLib;

namespace CNCZipper
{
    using DataLayer;

    public partial class ctrlZippCycles : UserControl, IControlData
    {
        private ZippCycle cycle = null;

        public string GetTitle() { return Global.GetText("TxtZippCycles"); }
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public void SetActive(bool flag) {}
        public void Update() {}


        public void UndoChanges()
        {
            if (cycle == null || Options.cycles.MODIFIED)
            {
                if (Global.ReadZippCycleDB())
                    SetCyclesModified(false);
            }
            else
            {
                if (Global.ReadZippCycleDB(cycle))
                    SetStepsModified(false);
            }
        }

        void SetStepsModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridCycleSteps.UnselectAll();
            gridCycleSteps.Items.Refresh();
        }

        void SetCyclesModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridCycles.UnselectAll();
            gridCycles.Items.Refresh();
            gridCycleSteps.UnselectAll();
            gridCycleSteps.Items.Refresh();
        }

        public ctrlZippCycles()
        {
            InitializeComponent();
            gridCycleSteps.ItemsSource = null;
            gridCycles.ItemsSource= Options.cycles.DATA;
            SetCyclesModified(false);
            SetStepsModified(false);
        }

        private void gridCycle_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridCycles.CurrentCell);

            if (cent == null)
                return;

            bool done = false;

            if (cent.INDEX == 1)
                done = dlgEntry.ReadData(Window.GetWindow(this), "TxtCycleValuePrompt", cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetCyclesModified(true);
            }
        }


        private void gridCycleSteps_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridCycleSteps.CurrentCell);
            if (cent == null)
                return;

            var owner = Window.GetWindow(this);
            var prompt_key = "TxtCycleStepValuePrompt";

            bool done = false;

            if (cent.INDEX == 5)
                done = dlgEntry.ReadData(owner, prompt_key, Options.mcodes.OPT, cent, "MCODE");
            else if (cent.INDEX >= 0 && cent.INDEX < 5)
                done = dlgEntry.ReadData(owner, prompt_key, cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetStepsModified(true);
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (cycle == null || Options.cycles.MODIFIED)
            {
                Global.SaveZippCycleDB();
                Global.ReadZippCycleDB();
                SetCyclesModified(false);
            }
            else
            {
                Global.SaveZippCycleDB(cycle);
                Global.ReadZippCycleDB(cycle);
                SetStepsModified(false);
            }
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        private void chkReRoll_Checked(object sender, RoutedEventArgs e)
        {
            if (cycle != null)
                gridCycleSteps.ItemsSource = cycle.roll_steps.DATA;
        }

        private void chkReRoll_Unchecked(object sender, RoutedEventArgs e)
        {
            if (cycle != null)
                gridCycleSteps.ItemsSource = cycle.full_steps.DATA;
        }

        private void DeleteStep(object sender, RoutedEventArgs e)
        {
            var obj = gridCycleSteps.SelectedItem;
            if (cycle != null && obj != null && obj.GetType() == typeof(ZippCycleDtl))
            {
                var steps = (chkReRoll.IsChecked.Value ? cycle.roll_steps : cycle.full_steps);
                var step_to_delete = (ZippCycleDtl)obj;

                if (step_to_delete.STATUS == EntityStatus.New)
                    steps.RemoveRecount(step_to_delete);
                else
                    step_to_delete.STATUS = EntityStatus.Del;

                SetStepsModified(true);
            }
        }

        private void InsertStepCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridCycleSteps.SelectedItem;
            if (cycle != null && obj != null && obj.GetType() == typeof(ZippCycleDtl))
            {
                var steps = (chkReRoll.IsChecked.Value ? cycle.roll_steps : cycle.full_steps);
                var step_to_copy = (ZippCycleDtl)obj;
                steps.InsertClone(step_to_copy);
                SetStepsModified(true);
            }
        }

        private void AddStepCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridCycleSteps.SelectedItem;
            if (cycle != null && obj != null && obj.GetType() == typeof(ZippCycleDtl))
            {
                var steps = (chkReRoll.IsChecked.Value ? cycle.roll_steps : cycle.full_steps);
                var obj_to_copy = (ZippCycleDtl)obj;
                var obj_new = (ZippCycleDtl)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                steps.AddNew(obj_new);
                SetStepsModified(true);
            }
        }

        private void InsertStepNew(object sender, RoutedEventArgs e)
        {
            var obj = gridCycleSteps.SelectedItem;
            if (cycle != null && obj != null && obj.GetType() == typeof(ZippCycleDtl))
            {
                var steps = (chkReRoll.IsChecked.Value ? cycle.roll_steps : cycle.full_steps);
                var obj_sel = (ZippCycleDtl)obj;

                var obj_new = new ZippCycleDtl();
                obj_new.CYCLE_ID = obj_sel.CYCLE_ID;
                steps.InsertAfter(obj_sel, obj_new);
                SetStepsModified(true);
            }
        }

        private void AddStepNew(object sender, RoutedEventArgs e)
        {
            if (cycle == null)
                return;

            var obj_new = new ZippCycleDtl();
            obj_new.CYCLE_ID = cycle.ID;
            var steps = (chkReRoll.IsChecked.Value ? cycle.roll_steps : cycle.full_steps);
            steps.AddNew(obj_new);
            SetStepsModified(true);
        }

        private void CycleDelete(object sender, RoutedEventArgs e)
        {
            var obj = gridCycles.SelectedItem;
            if (obj != null && obj.GetType() == typeof(ZippCycle))
            {
                var obj_to_delete = (ZippCycle)obj;

                if (obj_to_delete.STATUS == EntityStatus.New)
                    Options.cycles.Remove(obj_to_delete);
                else
                    obj_to_delete.STATUS = EntityStatus.Del;

                SetCyclesModified(true);
            }
        }

        private void CycleCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridCycles.SelectedItem;
            if (obj != null && obj.GetType() == typeof(ZippCycle))
            {
                var obj_to_copy = (ZippCycle)obj;
                var obj_new = (ZippCycle)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                Options.cycles.AddNew(obj_new);
                obj_new.SetID();
                SetCyclesModified(true);
            }
        }

        private void CycleNew(object sender, RoutedEventArgs e)
        {
            var obj_new = new ZippCycle();
            Options.cycles.AddNew(obj_new);
            SetCyclesModified(true);
        }

        private void gridCycles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cycle = (ZippCycle)gridCycles.SelectedItem;
            if (cycle != null)
                gridCycleSteps.ItemsSource = (chkReRoll.IsChecked.Value ? cycle.roll_steps.DATA : cycle.full_steps.DATA);
            else
                gridCycleSteps.ItemsSource = null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CNCLib;

namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlAssemblies.xaml
    /// </summary>
    public partial class ctrlAssemblies : UserControl, IControlData
    {
        public string GetTitle() { return Global.GetText("TxtAssemblies"); }
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public void SetActive(bool flag) {}
        public void Update() {}

        public void UndoChanges()
        {
            if (Global.ReadAssembliesDB())
                SetModified(false);
        }

        void SetModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridAssemblies.UnselectAll();
            gridAssemblies.Items.Refresh();
        }

        public ctrlAssemblies()
        {
            InitializeComponent();
            gridAssemblies.ItemsSource = Options.assemblies.DATA;
            SetModified(false);
            brdDrawing.Visibility = Visibility.Hidden;
        }

        private void gridAssemblies_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridAssemblies.CurrentCell);
            if (cent == null)
                return;

            var owner = Window.GetWindow(this);
            var prompt_key = "TxtAssemblyValuePrompt";

            bool done = false;

            if(cent.INDEX == 2)
                done = dlgEntry.ReadData(owner, prompt_key, Options.fixtures.OPT, cent, "FIXTURE_ID");
            else if(cent.INDEX == 5)
                done = dlgEntry.ReadData(owner, prompt_key, Options.struts.OPT, cent, "STRUT_ID");
            else if (cent.INDEX == 12)
                done = dlgEntry.ReadData(owner, prompt_key, Options.cycles.OPT, cent, "CYCLE_ID");
            else if (cent.INDEX == 15)
                done = dlgEntry.ReadData(owner, prompt_key, Options.knurling.OPT, cent, "KNURLING_SET_ID");
            else if (cent.INDEX == 16)
                done = dlgEntry.ReadData(owner, prompt_key, Options.rolls.OPT, cent, "ROLL_SET_ID");
            else if(cent.INDEX > 0 && cent.INDEX < 27)
                done = dlgEntry.ReadData(owner, prompt_key, cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetModified(true);
            }
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Global.SaveAssembliesDB();
            Global.ReadAssembliesDB();

            SetModified(false);
        }

        private void Delete(object sender, RoutedEventArgs e)
        {
            var obj = gridAssemblies.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Assembly))
            {
                var obj_to_delete = (Assembly)obj;

                if (obj_to_delete.STATUS == EntityStatus.New)
                    Options.assemblies.Remove(obj_to_delete);
                else
                    obj_to_delete.STATUS = EntityStatus.Del;

                SetModified(true);
            }
        }

        private void Copy(object sender, RoutedEventArgs e)
        {
            var obj = gridAssemblies.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Assembly))
            {
                var obj_to_copy = (Assembly)obj;
                var obj_new = (Assembly)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                Options.assemblies.AddNew(obj_new);
                SetModified(true);
            }
        }

        private void New(object sender, RoutedEventArgs e)
        {
            var obj_new = new Assembly();
            Options.assemblies.AddNew(obj_new);
            SetModified(true);
        }

        private void gridAssemblies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var obj = gridAssemblies.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Assembly))
            {
                brdDrawing.Visibility = Visibility.Visible;
                var obj_to_show = (Assembly)obj;
                imgDrawing.Source = obj_to_show.IMAGE;
            }
        }
    }
}
﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Windows;

using CNCLib;
namespace CNCZipper
{
    using DataLayer;
    using System.Collections.Generic;
    public class Global
    {
        private static DbBase db = null;
        private static DbClient dbClient = null;

        public static NameValueCollection settings = null;

        public static ZipperPLC PLC = null;
        private static UserData user = null;
        public static Assembly assembly = null;

        public static bool Logon(string usr, string pwd)
        {
            foreach(var u in Options.users.DATA)
            {
                if((usr.Length == 0 || u.NAME.Equals(usr)) && u.PASS.Equals(pwd))
                {
                    user = u;
                    return true;
                }
            }
            return false;
        }

        public static void Logout()
        {
            user = null;
        }

        public static string GetUser()
        {
            return user == null? string.Empty : user.NAME;
        }

        public static bool IsUserLogged()
        {
            return user != null;
        }

        public static string GetText(string key)
        {
            return (string)System.Windows.Application.Current.Resources[key];
        }

        private static void HandleException(Exception ex)
        {
            Logger.Write(ex.Message);
            Logger.Write(ex.StackTrace);
            MessageBox.Show(ex.Message);
            MessageBox.Show(ex.StackTrace);
        }

        //Strut DB -----------------------------------
        public static bool ReadStrutDB()
        {
            try
            {
                new DbStrut(db).Read(Options.struts);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveStrutDB()
        {
            try
            {
                new DbStrut(db).Save(Options.struts);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        //Parameters DB -----------------------------------
        public static bool ReadParamDB()
        {
            try
            {
                new DbParameters(db).Read(Options.parameters);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveParamDB()
        {
            try
            {
                new DbParameters(db).Save(Options.parameters);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        //AssemblyData DB -----------------------------------

        public static bool ReadAssembliesDB()
        {
            try
            {
                new DbAssembly(db).Read(Options.assemblies);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveAssembliesDB()
        {
            try
            {
                new DbAssembly(db).Save(Options.assemblies);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        // Fixture DB -----------------------------------
        public static bool ReadFixtureDB()
        {
            try
            {
                new DbFixture(db).Read(Options.fixtures);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveFixtureDB()
        {
            try
            {
                new DbFixture(db).Save(Options.fixtures);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        // ZippCycle DB -----------------------------------
        public static bool ReadZippCycleDB()
        {
            try
            {
                new DbZippCycle(db).Read(Options.cycles);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveZippCycleDB()
        {
            try
            {
                new DbZippCycle(db).Save(Options.cycles);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        // ZippCycle DB -----------------------------------
        public static bool ReadZippCycleDB(ZippCycle cycle)
        {
            try
            {
                new DbZippCycle(db).ReadCycleDtl(cycle);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveZippCycleDB(ZippCycle cycle)
        {
            try
            {
                new DbZippCycle(db).Save(cycle);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        // Knurling DB -----------------------------------
        public static bool ReadKnurlingDB()
        {
            try
            {
                new DbKnurling(db).Read(Options.knurling);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveKnurlingDB()
        {
            try
            {
                new DbKnurling(db).Save(Options.knurling);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        // Rolls DB -----------------------------------
        public static bool ReadRollDB()
        {
            try
            {
                new DbRoll(db).Read(Options.rolls);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static bool SaveRollDB()
        {
            try
            {
                new DbRoll(db).Save(Options.rolls);
                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        public static void SaveForClient(ClientEntry ce)
        {
            if (dbClient != null)
                dbClient.Insert(ce);
        }

        public static List<Entity> GetClientJobs()
        {
            if (dbClient != null)
                return dbClient.Jobs.OPT;
            else
                return new List<Entity>();
        }

        public static string GetClientJobName(int job_id)
        {
            var job = dbClient.Jobs.Find(job_id);
            return job == null ? "?" : job.NAME;
        }

        //All DB Data -----------------------------------
        public static bool Init(DbBase db1, DbBase db2, NameValueCollection config)
        {
            try
            {
                db = db1;

                if(db2 != null)
                {
                    var tmp = new DbClient(db2);

                    if (tmp.Init())
                        dbClient = tmp;
                }

                settings = config;

                Entity.METRIC = Boolean.Parse(settings["metric"]);
                Size sz = new Size();
                sz.Width = Int32.Parse(settings["panel_width"]);
                sz.Height = Int32.Parse(settings["panel_height"]);
                Application.Current.Resources["SubformSize"] = sz;

                LoadLanguage();

                TouchKeys.SetPath(settings["keyboard"]);

                bool emulate = Boolean.Parse(settings["emulate"]);
                PLC = new ZipperPLC(emulate);

                new DbMCode(db).Read(Options.mcodes);
                new DbAssembly(db).Read(Options.assemblies);
                new DbStrut(db).Read(Options.struts);
                new DbParameters(db).Read(Options.parameters);
                new DbAssembly(db).Read(Options.assemblies);
                new DbFixture(db).Read(Options.fixtures);
                new DbRoll(db).Read(Options.rolls);
                new DbKnurling(db).Read(Options.knurling);
                new DbZippCycle(db).Read(Options.cycles);
                new DbUser(db).Read(Options.users);
                new DbRoll(db).Read(Options.rolls);
                new DbMsgTable(db).Read(Options.msgs);

                if (Options.assemblies.Count > 0)
                    assembly = Options.assemblies.DATA[0];

                return true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return false;
            }
        }

        private static void LoadLanguage()
        {
            var info = new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ZipperText.txt"));

            if (!File.Exists(info.FullName))
                return;

            using (StreamReader sr = File.OpenText(info.FullName))
            {
                string line = String.Empty;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Length == 0 || line[0] == '#') // commented line
                        continue;

                    string[] key_value = line.Split('=');
                    if (key_value.Length == 2 && Application.Current.Resources.Contains(key_value[0]))
                        Application.Current.Resources[key_value[0]] = key_value[1];

                }
            }
        }
    }
}


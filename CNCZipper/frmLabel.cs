﻿using System;
using System.Windows.Forms;
using System.Drawing.Printing;


namespace CNCZipper
{
    public partial class frmLabel : Form, ILabel
    {
        public class Data
        {
            public int id;
            public string project;
            public string color;
            public string profile;
            public double length = 0.0;
            public int quantity = 0;
            public int counter = 1;

            public static Data Build(string[] arr)
            {
                var data = new Data();

                if (arr.Length < 3)
                    return null;

                var split = arr[2].Split(',');

                if (split.Length < 6)
                    return null;

                try
                {
                    data.project = split[0];
                    data.id = Int32.Parse(split[1].Trim());
                    data.color = split[2];
                    data.profile = split[3];
                    data.length = Double.Parse(split[4].Trim());
                    data.quantity = Int32.Parse(split[5].Trim());
                }
                catch
                {
                    return null;
                }

                return data;
            }
        }

        public frmLabel()
        {
            InitializeComponent();
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        }

        public void SetData(Object data)
        {
            var b = (frmLabel.Data)data;

            lblProject.Text = b.project;
            lblProfile.Text = b.profile;
            lblColor.Text =  b.color;
            lblLength.Text = String.Format("Length: {0}", b.length);
            lblCounter.Text = String.Format("{0} Of {1}", b.counter, b.quantity);
        }

        public PaperSize GetPaperSize()
        {
            return new PaperSize("Custom Paper Size", this.Width/2, this.Height/2);
        }
    }
}

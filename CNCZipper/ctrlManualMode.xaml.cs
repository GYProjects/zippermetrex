﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CNCLib;

namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlManualMode.xaml
    /// </summary>
    public partial class ctrlManualMode : UserControl, IControlData
    {
        public string GetTitle() { return Global.GetText("TxtManualMode"); }
        public bool IsModified() { return false; }
        public void UndoChanges() {}
        public void SetActive(bool active)
        {
            if (active)
            {
                Global.PLC.SetDevice("M1", 0);

                if (Global.assembly != null)
                    cmbAssembly.SelectedItem = Global.assembly;
            }
        }

        public void Update()
        {
            Global.PLC.ReadDeviceBlock_D100_32();
            Global.PLC.ReadOffset_D840_16();

            ShowTopFront(Global.PLC.PRegister[0], Global.PLC.PRegister[2]);
            ShowTopBack(Global.PLC.PRegister[1], Global.PLC.PRegister[3]);
            ShowBottomFront(Global.PLC.PRegister[4], Global.PLC.PRegister[6]);
            ShowBottomBack(Global.PLC.PRegister[5], Global.PLC.PRegister[7]);

            Switch(Global.PLC.SW5[3], ref SW5_3);
            Switch(Global.PLC.SW5[4], ref SW5_4);
            Switch(Global.PLC.SW5[0], ref SW5_0);
            Switch(Global.PLC.SW5[2], ref SW5_2);
            Switch(Global.PLC.SW5[1], ref SW5_1);

            Switch(Global.PLC.SW3[0], ref ledPedal_0);
            Switch(Global.PLC.SW3[1], ref SW3_1);
            Switch(Global.PLC.SW3[3], ref SW3_3);
            Switch(Global.PLC.SW3[2], ref SW3_2);
            Switch(Global.PLC.SW3[10], ref SW3_10);
            Switch(Global.PLC.SW3[11], ref SW3_11);
            Switch(Global.PLC.SW3[12], ref SW3_12);
            Switch(Global.PLC.SW3[7], ref SW3_7);
            Switch(Global.PLC.SW3[9], ref SW3_9);
            Switch(Global.PLC.SW3[5], ref SW3_5);
            Switch(Global.PLC.SW3[6], ref SW3_6);
            Switch(Global.PLC.SW3[4], ref SW3_4);
            Switch(Global.PLC.SW3[8], ref SW3_8);
            Switch(Global.PLC.SW3[15], ref ledCurtain_15);
            Switch(Global.PLC.SW3[13], ref ledStrut_13);

            //lblBackTablePos.Content = string.Format("{0:0.000}", Global.PLC.PRegister[9]);
            //lblFrontTablePos.Content = string.Format("{0:0.000}", Global.PLC.PRegister[10]);
            //lblWPos.Content = string.Format("{0:0.000}", Global.PLC.PRegister[8]);

            lblBackTablePos.Content = Entity.GetSizeStr(Global.PLC.PRegister[9]);
            lblFrontTablePos.Content = Entity.GetSizeStr(Global.PLC.PRegister[10]);
            lblWPos.Content = Entity.GetSizeStr(Global.PLC.PRegister[8]);
        }

        private Brush ColorOn;
        private Brush ColorOff;

        public ctrlManualMode()
        {
            InitializeComponent();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                this.Background = Brushes.Transparent;

            ColorOn = (Brush)FindResource("LEDColorON");
            ColorOff = (Brush)FindResource("LEDColorOFF");

            cmbAssembly.ItemsSource = Options.assemblies.DATA;
            cmbAssembly.DisplayMemberPath = "ID_NAME";
            cmbAssembly.SelectedValuePath = "ID";

            if (lblSpeedPercent != null)
            {
                int speed_percent = (int)SpeedScrollbar.Value;
                lblSpeedPercent.Content = string.Format(Global.GetText("TxtMotorSpeed"), speed_percent);
            }
        }

        private void Switch(bool state, ref Ellipse ellipse)
        {
            ellipse.Fill = state ? ColorOn : ColorOff;
        }

        private void SpeedScrollbar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int speed_percent = (int)e.NewValue;

            if (lblSpeedPercent != null)
            {
                lblSpeedPercent.Content = string.Format(Global.GetText("TxtMotorSpeed"), speed_percent);
            }

            Global.PLC.OverrideSpeed(speed_percent);
        }

        private void cmbAssembly_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var a = (Assembly)cmbAssembly.SelectedItem;

            if (a != null)
            {
                Global.assembly = a;
                Global.PLC.LoadAssembly(a);

                lblAssemblyAttr.Content = string.Format("Strut: {0}\nFixture: {1}\nRoll Set: {2}\nKnurling: {3}\nCycle: {4}",
                    a.STRUT, a.FIXTURE, a.ROLL_SET, a.KNURLING_SET, a.CYCLE);
            }
        }

        private void ShowTopFront(double x, double z)
        {
            ShowPosition(lblTopFrontPos, "X", x, "Z", z);
        }

        private void ShowTopBack(double x, double z)
        {
            ShowPosition(lblTopBackPos, "X", x, "Z", z);
        }

        private void ShowBottomFront(double x, double z)
        {
            ShowPosition(lblBottomFrontPos, "X", x, "Z", z);
        }

        private void ShowBottomBack(double x, double z)
        {
            ShowPosition(lblBottomBackPos, "X", x, "Z", z);
        }

        private void ShowPosition(Label lbl, string c1, double d1, string c2, double d2)
        {
            //lbl.Content = string.Format("{0}: {1:0.000}\n{2}: {3:0.000}", c1, d1, c2, d2);
            lbl.Content = string.Format("{0}: {1}\n{2}: {3}", c1, Entity.GetSizeStr(d1), c2, Entity.GetSizeStr(d2));
        }

        private void btnPosition_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var color = Brushes.White;
            Button btn = (Button)sender;
            ColorPositionLabel(btn, color);
            Global.PLC.StopMove();
            btn.ToolTip = btn.Tag.ToString();
        }

        private void ColorPositionLabel(Button btn, SolidColorBrush color)
        {
            if (btn.Name.StartsWith("btnTopFront"))
                lblTopFrontPos.Foreground = color;
            else if (btn.Name.StartsWith("btnTopBack"))
                lblTopBackPos.Foreground = color;
            else if (btn.Name.StartsWith("btnBottomFront"))
                lblBottomFrontPos.Foreground = color;
            else if (btn.Name.StartsWith("btnBottomBack"))
                lblBottomBackPos.Foreground = color;
            else if (btn.Name.StartsWith("btnBackTable"))
                lblBackTablePos.Foreground = color;
            else if (btn.Name.StartsWith("btnFrontTable"))
                lblFrontTablePos.Foreground = color;
            else if (btn.Name.StartsWith("btnW"))
                lblWPos.Foreground = color;
        }

        private void btnPosition_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var color = Brushes.Black;
            Button btn = (Button)sender;
            ColorPositionLabel(btn, color);
            Global.PLC.StartMove(Convert.ToInt32(btn.Tag));
        }

        private void btnToRollPos_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M20", 1);
        }

        private void btnToPrerollPos_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M18", 1);
        }

        private void btnToKnurlingPos_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M22", 1);
        }

        private void btnToggle_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            char c = btn.Content.ToString()[0];

            btn.ToolTip = btn.Tag.ToString();

            if(Global.PLC.Toggle_K8Y20(Convert.ToInt32(btn.Tag)))
            {
                if (c == '3')
                    btn.Content = "4";
                else if (c == '4')
                    btn.Content = "3";
                else if (c == '5')
                    btn.Content = "6";
                else if (c == '6')
                    btn.Content = "5";
                else if (c == 'i')
                    btn.Content = "r";
                else if (c == 'r')
                    btn.Content = "i";
            }
        }
    }

}

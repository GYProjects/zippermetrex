﻿using System;
using System.Data;
using CNCLib;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace CNCZipper.DataLayer
{
    public class ClientEntry
    {
        public DateTime st { get; set; } = DateTime.MinValue;
        public DateTime ft { get; set; } = DateTime.MinValue;
        public int employee { get; set; } = 0;
        public string profileID { get; set; } = "?";
        public int job_id { get; set; } = 0;
        public string job { get; set; } = "?";
        public int str { get; set; } = 0; //strutdelay
    }

    public class DbClient : DbBase
    {
        private readonly static Object _lock = new Object();
        private int last_id = -1;

        public DataMap<Entity> Jobs = new DataMap<Entity>();

        public DbClient(DbBase db)
            : base(db)
        {
        }

        //--------------------------------------------------------------------------------------
        public bool Init()
        {
            last_id = GetLastID();

            if (last_id < 0)
                return false;

            return ReadJobs();
        }

        //--------------------------------------------------------------------------------------
        private int GetLastID()
        {
            int max_id = -1;
            IDbCommand cmd = GetDbCommand("select max(ID) from prozipperRed");
            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                if (rdr.Read())
                    max_id = ReadInt32(rdr, 0);

                Logger.Write("DbClient.GetLastID() returned [{0}]", max_id);
            }
            catch (Exception ex)
            {
                Logger.Write("DbClient.GetLastID() Failed!");
                Logger.Write(ex.Message);
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }

            return max_id;
        }


        //--------------------------------------------------------------------------------------
        private bool ReadJobs()
        {
            bool res = false;
            IDbCommand cmd = GetDbCommand("select ID, project_name from prozipperRedprojects order by ID");
  
            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Entity job = new Entity();
                    job.ID = ReadInt32(rdr, 0);
                    job.NAME = ReadString(rdr, 1);
                    Jobs.Add(job);
                }

                Logger.Write("DbClient.ReadJobs(): [{0}] jobs found", Jobs.Count);

                res = true;
            }
            catch (Exception ex)
            {
                Logger.Write("DbClient.ReadJobs() Failed!");
                Logger.Write(ex.Message);
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }

            return res;
        }

        public void Insert(ClientEntry ce)
        {
            string s = "insert into prozipperRed(ID, st, ft, employee, profileID, job, [day], [month], [year], str)";
            s += " values({0},#{1}#,#{2}#,{3},'{4}','{5}',{6},{7},{8},{9})";
            string ins = string.Format(s, ++last_id, ce.st.ToLongTimeString(), ce.ft.ToLongTimeString(), ce.employee, ce.profileID, ce.job_id, ce.st.Day, ce.st.Month, ce.st.Year, ce.str);

            Task myTask = new Task(() => RunQuery(ins));
            myTask.Start();
        }


        private void RunQuery(string statement)
        {
            lock (_lock)
            {
                IDbCommand cmd = GetDbCommand(statement);
                try
                {
                    OpenConnection();
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    Logger.Write("Statement failed! [{0}]", statement);
                }
                finally
                {
                    CloseConnection();
                }
            }
        }
    }
}

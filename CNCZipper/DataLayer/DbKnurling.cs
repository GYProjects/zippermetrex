﻿using System;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class Knurling : Entity
    {
        public double Z_CORRECTION
        {
            get { return GetSize(z_correction); }
            set { SetSize(value, out z_correction); }
        }

        public double z_correction = 0.0;
    }

    public class DbKnurling : DbEntityBase<Knurling>
    {
        public DbKnurling(DbBase db) : base(db, "KNURLING_SET")
        {
        }

        public sealed override void Read(DataMap<Knurling> knurl)
        {
            string select = "select ID, NAME, Z_CORRECTION from KNURLING_SET order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                knurl.Clear();

                while (rdr.Read())
                {
                    var data = new Knurling();
                    data.ID = ReadInt32(rdr, 0);
                    data.NAME = ReadString(rdr, 1);
                    data.z_correction = ReadDecimal(rdr, 2);
                    data.STATUS = EntityStatus.Set;
                    knurl.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }
        }

        protected sealed override void Update(Knurling data)
        {
            string str = String.Format("update KNURLING_SET set NAME='{0}', Z_CORRECTION={1} where ID={2}",
                 data.NAME, data.z_correction, data.ID);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        protected sealed override void Insert(Knurling data)
        {
            string str = String.Format("insert into KNURLING_SET(ID, NAME, Z_CORRECTION) values({0}, '{1}', {2})",
                data.ID, data.NAME, data.z_correction);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }
    }
}
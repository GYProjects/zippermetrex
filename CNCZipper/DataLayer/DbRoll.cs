﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CNCLib;
using System.Data;

namespace CNCZipper.DataLayer
{
    public class Roll : Entity
    {
        public double Z_CORRECTION
        {
            get { return GetSize(z_correction); }
            set { SetSize(value, out z_correction); }
        }
        public double z_correction = 0.0;
    }

    public class DbRoll : DbEntityBase<Roll>
    {

        //--------------------------------------------------------------------------------------
        public DbRoll(DbBase db)
            : base(db, "ROLL_SET")
        {
        }

        public sealed override void Read(DataMap<Roll> rolls)
        {
            string select = "select ID, NAME, Z_CORRECTION from ROLL_SET order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                rolls.Clear();

                while (rdr.Read())
                {
                    var data = new Roll();
                    data.ID = ReadInt32(rdr, 0);
                    data.NAME = ReadString(rdr, 1);
                    data.z_correction = ReadDecimal(rdr, 2);
                    data.STATUS = EntityStatus.Set;
                    rolls.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }
        }

        protected sealed override void Update(Roll data)
        {
            string str = String.Format("update ROLL_SET set NAME='{0}', Z_CORRECTION={1} where ID={2}",
                 data.NAME, data.z_correction, data.ID);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        protected sealed override void Insert(Roll data)
        {
            string str = String.Format("insert into ROLL_SET(ID, NAME, Z_CORRECTION) values({0}, '{1}', {2})",
                data.ID, data.NAME, data.z_correction);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }
    }
}
﻿using System;
using System.IO;
using System.Data;
using CNCLib;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace CNCZipper.DataLayer
{
    public class Assembly : Entity
    {
        public int FIXTURE_ID { get; set; } = 0;
        public string FIXTURE { get { return Options.fixtures.Get(FIXTURE_ID).NAME; } }
        public double bot_roll_height = 0.0;
        public double BOT_ROLL_HEIGHT
        {
            get { return GetSize(bot_roll_height); } set { SetSize(value, out bot_roll_height); }
        }
        public double top_roll_height = 0.0;
        public double TOP_ROLL_HEIGHT
        {
            get { return GetSize(top_roll_height); } set { SetSize(value, out top_roll_height); }
        }

        public int STRUT_ID { get; set; } = 0;
        public string STRUT { get { return Options.struts.Get(STRUT_ID).NAME; } }
        public double stat_table = 0.0;
        public double STAT_TABLE
        {
            get { return GetSize(stat_table); } set { SetSize(value, out stat_table); }
        }
        public double move_table = 0.0;
        public double MOVE_TABLE
        {
            get { return GetSize(move_table); } set { SetSize(value, out move_table); }
        }
        public double front_offset_top = 0.0;
        public double FRONT_OFFSET_TOP
        {
            get { return GetSize(front_offset_top); } set { SetSize(value, out front_offset_top); }
        }
        public double back_offset_top = 0.0;
        public double BACK_OFFSET_TOP
        {
            get { return GetSize(back_offset_top); } set { SetSize(value, out back_offset_top); }
        }
        public double front_offset_bot = 0.0;
        public double FRONT_OFFSET_BOT
        {
            get { return GetSize(front_offset_bot); } set { SetSize(value, out front_offset_bot); }
        }
        public double back_offset_bot = 0.0;
        public double BACK_OFFSET_BOT
        {
            get { return GetSize(back_offset_bot); } set { SetSize(value, out back_offset_bot); }
        }

        public int CYCLE_ID { get; set; } = 0;
        public string CYCLE { get { return Options.cycles.Get(CYCLE_ID).NAME; } }
        public double top_knurling_vert = 0.0;
        public double TOP_KNURLING_VERT
        {
            get { return GetSize(top_knurling_vert); } set { SetSize(value, out top_knurling_vert); }
        }
        public double bot_knurling_vert = 0.0;
        public double BOT_KNURLING_VERT
        {
            get { return GetSize(bot_knurling_vert); } set { SetSize(value, out bot_knurling_vert); }
        }
        public int KNURLING_SET_ID { get; set; } = 0;
        public string KNURLING_SET { get { return Options.knurling.Get(KNURLING_SET_ID).NAME; } }
        public int ROLL_SET_ID { get; set; } = 0;
        public string ROLL_SET { get { return Options.rolls.Get(ROLL_SET_ID).NAME; } }
        public double preroll_horz = 0.0;
        public double PREROLL_HORZ
        {
            get { return GetSize(preroll_horz); } set { SetSize(value, out preroll_horz); }
        }
        public double preroll_vert = 0.0;
        public double PREROLL_VERT
        {
            get { return GetSize(preroll_vert); } set { SetSize(value, out preroll_vert); }
        }

        public int top_left_knurling_psi = 0;
        public int TOP_LEFT_KNURLING_PSI
        {
            get { return GetPressure(top_left_knurling_psi); }
            set { SetPressure(value, out top_left_knurling_psi); }
        }
        public int top_right_knurling_psi = 0;
        public int TOP_RIGHT_KNURLING_PSI
        {
            get { return GetPressure(top_right_knurling_psi); }
            set { SetPressure(value, out top_right_knurling_psi); }
        }
        public int bottom_left_knurling_psi = 0;
        public int BOTTOM_LEFT_KNURLING_PSI
        {
            get { return GetPressure(bottom_left_knurling_psi); }
            set { SetPressure(value, out bottom_left_knurling_psi); }
        }
        public int bottom_right_knurling_psi = 0;
        public int BOTTOM_RIGHT_KNURLING_PSI
        {
            get { return GetPressure(bottom_right_knurling_psi); }
            set { SetPressure(value, out bottom_right_knurling_psi); }
        }
        public int front_roll_psi = 0;
        public int FRONT_ROLL_PSI
        {
            get { return GetPressure(front_roll_psi); }
            set { SetPressure(value, out front_roll_psi); }
        }
        public int back_roll_psi = 0;
        public int BACK_ROLL_PSI
        {
            get { return GetPressure(back_roll_psi); }
            set { SetPressure(value, out back_roll_psi); }
        }

        public double stat_table_unload = 0.0;
        public double STAT_TABLE_UNLOAD
        {
            get { return GetSize(stat_table_unload); } set { SetSize(value, out stat_table_unload); }
        }
        public double move_table_unload = 0.0;
        public double MOVE_TABLE_UNLOAD
        {
            get { return GetSize(move_table_unload); } set { SetSize(value, out move_table_unload); }
        }

        public BitmapImage IMAGE { get; set; } = null;
    }


    class DbAssembly : DbEntityBase<Assembly>
    {
        private const string COLUMMS = "ID, NAME, FIXTURE_ID, BOT_ROLL_HEIGHT, TOP_ROLL_HEIGHT, STRUT_ID," +
        " STAT_TABLE, MOVE_TABLE, FRONT_OFFSET_TOP, BACK_OFFSET_TOP, FRONT_OFFSET_BOT, BACK_OFFSET_BOT," +
           " CYCLE_ID, TOP_KNURLING_VERT, BOT_KNURLING_VERT, KNURLING_SET_ID, ROLL_SET_ID, PREROLL_HORZ, PREROLL_VERT," +
            "TOP_LEFT_KNURLING_PSI, TOP_RIGHT_KNURLING_PSI, BOTTOM_LEFT_KNURLING_PSI, BOTTOM_RIGHT_KNURLING_PSI, FRONT_ROLL_PSI, BACK_ROLL_PSI, STAT_TABLE_UNLOAD, MOVE_TABLE_UNLOAD";

        //--------------------------------------------------------------------------------------
        public DbAssembly(DbBase db)
            : base(db, "ASSEMBLY")
        {
        }

        public sealed override void Read(DataMap<Assembly> assemblies)
        {
            string select = "select " + COLUMMS + " from " + TABLE + " order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                assemblies.Clear();

                while (rdr.Read())
                {
                    Assembly data = new Assembly();
                    int i = 0;
                    data.ID = ReadInt32(rdr, i++);
                    data.NAME = ReadString(rdr, i++);
                    data.FIXTURE_ID = ReadInt32(rdr, i++);
                    data.bot_roll_height = ReadDecimal(rdr, i++);
                    data.top_roll_height = ReadDecimal(rdr, i++);
                    data.STRUT_ID = ReadInt32(rdr, i++);
                    data.stat_table = ReadDecimal(rdr, i++);
                    data.move_table = ReadDecimal(rdr, i++);
                    data.front_offset_top = ReadDecimal(rdr, i++);
                    data.back_offset_top = ReadDecimal(rdr, i++);
                    data.front_offset_bot = ReadDecimal(rdr, i++);
                    data.back_offset_bot = ReadDecimal(rdr, i++);
                    data.CYCLE_ID = ReadInt32(rdr, i++);
                    data.top_knurling_vert = ReadDecimal(rdr, i++);
                    data.bot_knurling_vert = ReadDecimal(rdr, i++);
                    data.KNURLING_SET_ID = ReadInt32(rdr, i++);
                    data.ROLL_SET_ID = ReadInt32(rdr, i++);
                    data.preroll_horz = ReadDecimal(rdr, i++);
                    data.preroll_vert = ReadDecimal(rdr, i++);
                    data.top_left_knurling_psi = ReadInt32(rdr, i++);
                    data.top_right_knurling_psi = ReadInt32(rdr, i++);
                    data.bottom_left_knurling_psi = ReadInt32(rdr, i++);
                    data.bottom_right_knurling_psi = ReadInt32(rdr, i++);
                    data.front_roll_psi = ReadInt32(rdr, i++);
                    data.back_roll_psi = ReadInt32(rdr, i++);
                    data.stat_table_unload = ReadDecimal(rdr, i++);
                    data.move_table_unload = ReadDecimal(rdr, i++);
                    data.STATUS = EntityStatus.Set;
                    assemblies.Add(data);

                    SetImage(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }

            Logger.Write("Assembly [{0}] entries loaded", assemblies.Count);
        }

        protected sealed override void Update(Assembly a)
        {
            string fmt = "update ASSEMBLY set NAME='{0}', FIXTURE_ID={1}, BOT_ROLL_HEIGHT={2}, TOP_ROLL_HEIGHT={3}, STRUT_ID={4},";
            fmt += " STAT_TABLE={5}, MOVE_TABLE={6}, FRONT_OFFSET_TOP={7}, BACK_OFFSET_TOP={8}, FRONT_OFFSET_BOT={9}, BACK_OFFSET_BOT={10},";
            fmt += " CYCLE_ID={11}, TOP_KNURLING_VERT={12}, BOT_KNURLING_VERT={13}, KNURLING_SET_ID={14}, ROLL_SET_ID={15}, PREROLL_HORZ={16}, PREROLL_VERT={17},";
            fmt += " TOP_LEFT_KNURLING_PSI={18}, TOP_RIGHT_KNURLING_PSI={19}, BOTTOM_LEFT_KNURLING_PSI={20}, BOTTOM_RIGHT_KNURLING_PSI={21}, FRONT_ROLL_PSI={22}, BACK_ROLL_PSI={23}, STAT_TABLE_UNLOAD={24}, MOVE_TABLE_UNLOAD={25}";
            fmt += " where ID={26}";

            string upd = string.Format(fmt, a.NAME, a.FIXTURE_ID, a.bot_roll_height, a.top_roll_height, a.STRUT_ID,
                a.stat_table, a.move_table, a.front_offset_top, a.back_offset_top, a.front_offset_bot, a.back_offset_bot,
                a.CYCLE_ID, a.top_knurling_vert, a.bot_knurling_vert, a.KNURLING_SET_ID, a.ROLL_SET_ID, a.preroll_horz, a.preroll_vert,
                a.top_left_knurling_psi, a.top_right_knurling_psi, a.bottom_left_knurling_psi, a.bottom_right_knurling_psi, a.front_roll_psi, a.back_roll_psi, a.stat_table_unload, a.move_table_unload, a.ID);

            IDbCommand cmd = GetDbCommand(upd);
            cmd.ExecuteNonQuery();
        }

        protected sealed override void Insert(Assembly a)
        {
            string fmt = "insert into ASSEMBLY(ID, NAME, FIXTURE_ID, BOT_ROLL_HEIGHT, TOP_ROLL_HEIGHT, STRUT_ID,";
            fmt += " STAT_TABLE, MOVE_TABLE, FRONT_OFFSET_TOP, BACK_OFFSET_TOP, FRONT_OFFSET_BOT, BACK_OFFSET_BOT,";
            fmt += " CYCLE_ID, TOP_KNURLING_VERT, BOT_KNURLING_VERT, KNURLING_SET_ID, ROLL_SET_ID, PREROLL_HORZ, PREROLL_VERT,";
            fmt += " TOP_LEFT_KNURLING_PSI, TOP_RIGHT_KNURLING_PSI, BOTTOM_LEFT_KNURLING_PSI, BOTTOM_RIGHT_KNURLING_PSI, FRONT_ROLL_PSI, BACK_ROLL_PSI, STAT_TABLE_UNLOAD, MOVE_TABLE_UNLOAD)";
            fmt += " values({0},'{1}',{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},'{13}',{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26})";

            string upd = string.Format(fmt, a.ID, a.NAME, a.FIXTURE_ID, a.bot_roll_height, a.top_roll_height, a.STRUT_ID,
                a.stat_table, a.move_table, a.front_offset_top, a.back_offset_top, a.front_offset_bot, a.back_offset_bot,
                a.CYCLE_ID, a.top_knurling_vert, a.bot_knurling_vert, a.KNURLING_SET_ID, a.ROLL_SET_ID, a.preroll_horz, a.preroll_vert,
                a.top_left_knurling_psi, a.top_right_knurling_psi, a.bottom_left_knurling_psi, a.bottom_right_knurling_psi, a.front_roll_psi, a.back_roll_psi, a.stat_table_unload, a.move_table_unload);

            IDbCommand cmd = GetDbCommand(upd);
            cmd.ExecuteNonQuery();
        }

        public static void SetImage(Assembly a)
        {
            string img_file = string.Format("Drawings{0}A{1}.bmp", Path.DirectorySeparatorChar, a.ID);
            var info = new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, img_file));

            try
            {
                a.IMAGE = new BitmapImage(new Uri(info.FullName));
            }
            catch
            {
                Bitmap bmp = new Bitmap(600, 500);
                Graphics graphics = Graphics.FromImage(bmp);

                string message = string.Format(Global.GetText("TxtNoDrawing"), a.ID_NAME);
                graphics.DrawString(message, new Font("Verdana", 25), new SolidBrush(Color.DarkGray), new PointF(10.0F, 10.0F));

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ms.Position = 0;
                a.IMAGE = new BitmapImage();
                a.IMAGE.BeginInit();
                a.IMAGE.StreamSource = ms;
                a.IMAGE.EndInit();
            }
        }
    }
}

﻿using System;
using System.Data;
using CNCLib;
using System.Collections.Generic;

namespace CNCZipper.DataLayer
{
    public class ZippCycle : Entity
    {
        public override Object Clone()
        {
            ZippCycle clone = (ZippCycle) this.MemberwiseClone();

            clone.full_steps = new DataMap<ZippCycleDtl>();

            foreach(var s in full_steps.DATA)
            {
                var n = (ZippCycleDtl) s.Clone();
                n.STATUS = EntityStatus.New;
                clone.full_steps.AddNew(n);
            }

            clone.roll_steps = new DataMap<ZippCycleDtl>();

            foreach (var s in roll_steps.DATA)
            {
                var n = (ZippCycleDtl)s.Clone();
                n.STATUS = EntityStatus.New;
                clone.roll_steps.AddNew(n);
            }

            return clone;
        }

        public DataMap<ZippCycleDtl> full_steps = new DataMap<ZippCycleDtl>();
        public DataMap<ZippCycleDtl> roll_steps = new DataMap<ZippCycleDtl>();

        public void SetID()
        {
            foreach (var s in full_steps.DATA)
                s.CYCLE_ID = ID;

            foreach (var s in roll_steps.DATA)
                s.CYCLE_ID = ID;
        }
    }



    class DbZippCycle : DbBase
    {
    
        private DbFullZippCycleDtl db_full_step;
        private DbRollZippCycleDtl db_roll_step;

        //--------------------------------------------------------------------------------------
        public DbZippCycle(DbBase db)
            : base(db)
        {
            db_full_step = new DbFullZippCycleDtl(db);
            db_roll_step = new DbRollZippCycleDtl(db);
        }

        public void Read(DataMap<ZippCycle> cycles)
        {
            try
            {
                OpenConnection();
                cycles.Clear();
                ReadCycle(cycles);

                foreach (var cycle in cycles.DATA)
                {
                    db_full_step.Read(cycle.ID, cycle.full_steps);
                    db_roll_step.Read(cycle.ID, cycle.roll_steps);
                }
            }
            finally
            {
                CloseConnection();
            }
        }

        public void ReadCycleDtl(ZippCycle cycle)
        {
            try
            {
                OpenConnection();
                db_full_step.Read(cycle.ID, cycle.full_steps);
                db_roll_step.Read(cycle.ID, cycle.roll_steps);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void ReadCycle(DataMap<ZippCycle> cycles)
        {
            string select = "select ID, NAME from CYCLE order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ZippCycle data = new ZippCycle();
                    data.ID = ReadInt32(rdr, 0);
                    data.NAME = ReadString(rdr, 1);
                    data.STATUS = EntityStatus.Set;
                    cycles.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
            }

            Logger.Write("Cycle [{0}] entries loaded", cycles.Count);
        }

        private void Update(ZippCycle cycle)
        {
            string str = String.Format("update CYCLE set NAME='{0}' where ID={1}", cycle.NAME, cycle.ID);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
            cycle.STATUS = EntityStatus.Set;
        }

        private void Insert(ZippCycle cycle)
        {
            string str = String.Format("insert into CYCLE(ID, NAME) values({0}, '{1}')", cycle.ID, cycle.NAME);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
            cycle.STATUS = EntityStatus.Set;
        }

        private void Delete(ZippCycle cycle)
        {
            string str = String.Format("delete from CYCLE where ID={0}", cycle.ID);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        private void SaveCycle(ZippCycle cycle)
        {
            db_full_step.DeleteAll(cycle.ID);
            db_roll_step.DeleteAll(cycle.ID);

            if (cycle.STATUS == EntityStatus.New)
                Insert(cycle);
            else if (cycle.STATUS == EntityStatus.Upd)
                Update(cycle);
            else if (cycle.STATUS == EntityStatus.Del)
                Delete(cycle);

            if (cycle.STATUS != EntityStatus.Del)
            {
                var full_steps = new List<ZippCycleDtl>(cycle.full_steps.DATA);
                foreach (var step in full_steps)
                    if (step.STATUS != EntityStatus.Del)
                        db_full_step.Insert(step);
                    else
                        cycle.full_steps.RemoveRecount(step);

                var roll_steps = new List<ZippCycleDtl>(cycle.roll_steps.DATA);
                foreach (var step in roll_steps)
                    if (step.STATUS != EntityStatus.Del)
                        db_roll_step.Insert(step);
                    else
                        cycle.roll_steps.RemoveRecount(step);
            }
        }

        public void Save(ZippCycle cycle)
        {
            try
            {
                OpenConnection();
                BeginTransaction();
                SaveCycle(cycle);
                CommitTransaction();
            }
            catch (Exception)
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Save(DataMap<ZippCycle> cycles)
        {
            foreach (var cycle in cycles.DATA)
                Save(cycle);
        }
    }
}

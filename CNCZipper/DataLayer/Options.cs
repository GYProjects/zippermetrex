﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class Options
    {
        public static DataMap<Strut> struts = new DataMap<Strut>();
        public static DataMap<Assembly> assemblies = new DataMap<Assembly>();
        public static DataMap<Fixture> fixtures = new DataMap<Fixture>();
        public static DataMap<ParamData> parameters = new DataMap<ParamData>();
        public static DataMap<Knurling> knurling = new DataMap<Knurling>();
        public static DataMap<Roll> rolls = new DataMap<Roll>();
        public static DataMap<MCode> mcodes = new DataMap<MCode>();
        public static DataMap<ZippCycle> cycles = new DataMap<ZippCycle>();
        public static DataMap<UserData> users = new DataMap<UserData>();
        public static DataMap<ErrorMsg> msgs = new DataMap<ErrorMsg>();
    }
}

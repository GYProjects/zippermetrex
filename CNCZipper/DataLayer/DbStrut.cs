﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class Strut : Entity
    {
        public double WIDTH
        {
            get { return GetSize(width); }
            set { SetSize(value,out width);}
        }
        public double width = 0.0;
    }

    class DbStrut : DbEntityBase<Strut>
    {
        //--------------------------------------------------------------------------------------
        public DbStrut(DbBase db)
            : base(db, "STRUT")
        {
        }

        public sealed override void Read(DataMap<Strut> struts)
        {
            string select = "select ID, NAME, WIDTH from STRUT order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                struts.Clear();

                while (rdr.Read())
                {
                    Strut data = new Strut();

                    data.ID = ReadInt32(rdr, 0);
                    data.NAME = ReadString(rdr, 1);
                    data.width = ReadDecimal(rdr, 2);
                    data.STATUS = EntityStatus.Set;
                    struts.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }

            Logger.Write("Strut [{0}] entries loaded", struts.Count);
        }

        protected sealed override void Update(Strut data)
        {
            string str = String.Format("update STRUT set NAME='{0}', WIDTH={1} where ID={2}",
                 data.NAME, data.width, data.ID);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        protected sealed override void Insert(Strut data)
        {
            string str = String.Format("insert into STRUT(ID, NAME, WIDTH) values({0}, '{1}', {2})", data.ID, data.NAME, data.width);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }
    }
}

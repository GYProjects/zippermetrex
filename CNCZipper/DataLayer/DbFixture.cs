﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class Fixture : Entity
    {
        public double height = 0.0;
        public double HEIGHT
        {
            get { return GetSize(height); }
            set { SetSize(value, out height); }
        }
        public string STAT_TABLE_ID { get; set; }
        public double stat_table_width = 0.0;
        public double STAT_TABLE_WIDTH
        {
            get { return GetSize(stat_table_width); }
            set { SetSize(value, out stat_table_width); }
        }
        public double stat_table_height = 0.0;
        public double STAT_TABLE_HEIGHT
        {
            get { return GetSize(stat_table_height); }
            set { SetSize(value, out stat_table_height); }
        }
        public string FRONT_STAT_ID { get; set; }
        public string BACK_STAT_ID { get; set; }
        public string MOVE_TABLE_ID { get; set; }
        public double move_table_width = 0.0;
        public double MOVE_TABLE_WIDTH
        {
            get { return GetSize(move_table_width); }
            set { SetSize(value, out move_table_width); }
        }
        public double move_table_height = 0.0;
        public double MOVE_TABLE_HEIGHT
        {
            get { return GetSize(move_table_height); }
            set { SetSize(value, out move_table_height); }
        }
        public string FRONT_MOVE_ID { get; set; }
        public string BACK_MOVE_ID { get; set; }
    }

    class DbFixture : DbEntityBase<Fixture>
    {
        //--------------------------------------------------------------------------------------
        public DbFixture(DbBase db)
            : base(db, "FIXTURE")
        {
        }

        public sealed override void Read(DataMap<Fixture> fixtures)
        {
            string select = "select ID, NAME, STAT_TABLE_ID, STAT_TABLE_WIDTH, STAT_TABLE_HEIGHT, FRONT_STAT_ID, BACK_STAT_ID,"+
                " MOVE_TABLE_ID, MOVE_TABLE_WIDTH, MOVE_TABLE_HEIGHT, FRONT_MOVE_ID, BACK_MOVE_ID from FIXTURE order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                fixtures.Clear();

                while (rdr.Read())
                {
                    var data = new Fixture();
                    data.ID = ReadInt32(rdr, 0);
                    data.NAME = ReadString(rdr, 1);
                    data.STAT_TABLE_ID = rdr.GetString(2);
                    data.stat_table_width = ReadDecimal(rdr, 3);
                    data.stat_table_height = ReadDecimal(rdr, 4);
                    data.FRONT_STAT_ID = ReadString(rdr,5);
                    data.BACK_STAT_ID = ReadString(rdr, 6);
                    data.MOVE_TABLE_ID = ReadString(rdr, 7);
                    data.move_table_width = ReadDecimal(rdr, 8);
                    data.move_table_height = ReadDecimal(rdr, 9);
                    data.FRONT_MOVE_ID = ReadString(rdr, 10);
                    data.BACK_MOVE_ID = ReadString(rdr, 11);

                    data.STATUS = EntityStatus.Set;
                    fixtures.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }
        }

        protected sealed override void Update(Fixture data)
        {
            string str = String.Format("update FIXTURE set NAME='{0}', "+
                "STAT_TABLE_ID='{1}', STAT_TABLE_WIDTH={2}, STAT_TABLE_HEIGHT={3}, FRONT_STAT_ID='{4}', BACK_STAT_ID='{5}'," +
                "MOVE_TABLE_ID='{6}', MOVE_TABLE_WIDTH={7}, MOVE_TABLE_HEIGHT={8}, FRONT_MOVE_ID='{9}', BACK_MOVE_ID='{10}' where ID={11}", data.NAME,
                 data.STAT_TABLE_ID, data.stat_table_width, data.stat_table_height, data.FRONT_STAT_ID, data.BACK_STAT_ID,
                 data.MOVE_TABLE_ID, data.move_table_width, data.move_table_height, data.FRONT_MOVE_ID, data.BACK_MOVE_ID, data.ID);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        protected sealed override void Insert(Fixture data)
        {
            string str = String.Format("insert into FIXTURE(ID, NAME, " +
                "STAT_TABLE_ID, STAT_TABLE_WIDTH, STAT_TABLE_HEIGHT, FRONT_STAT_ID, BACK_STAT_ID, " +
                "MOVE_TABLE_ID, MOVE_TABLE_WIDTH, MOVE_TABLE_HEIGHT, FRONT_MOVE_ID, BACK_MOVE_ID) " +
                "values({0},'{1}','{2}',{3},{4},'{5}','{6}','{7}',{8},{9},'{10}','{11}')", data.ID, data.NAME,
                data.STAT_TABLE_ID, data.stat_table_width, data.stat_table_height, data.FRONT_STAT_ID, data.BACK_STAT_ID,
                data.MOVE_TABLE_ID, data.move_table_width, data.move_table_height, data.FRONT_MOVE_ID, data.BACK_MOVE_ID);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }
    }
}


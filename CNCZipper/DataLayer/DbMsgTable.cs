﻿using System;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class ErrorMsg : Entity
    {
        public string DESCRIPTION { get; set; }
        public string FATAL { get; set; }
        public string WARNING { get; set; }
    }


    public class DbMsgTable : DbEntityBase<ErrorMsg>
    {
        public DbMsgTable(DbBase db) : base(db, "MSG_TABLE")
        {
        }

        public sealed override void Read(DataMap<ErrorMsg> msgs)
        {
            string select = "select ID, NAME, ERROR_DESC, ERROR_FATAL, ERROR_WARN from MSG_TABLE order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                msgs.Clear();

                while (rdr.Read())
                {
                    int i = 0;
                    var data = new ErrorMsg();
                    data.ID = ReadInt32(rdr, i++);
                    data.NAME = ReadString(rdr, i++);
                    data.DESCRIPTION = ReadString(rdr, i++);
                    data.FATAL = ReadString(rdr, i++);
                    data.WARNING = ReadString(rdr, i++);
                    data.STATUS = EntityStatus.Set;
                    msgs.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }
        }

        protected override void Insert(ErrorMsg data)
        {
            throw new NotImplementedException();
        }

        protected override void Update(ErrorMsg data)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{

    public class ParamData : Entity
    {
        public enum Unit{ Length = 0, Speed = 1, Count = 2 }

        public double plc_value = 0.0;

        public double PLC_VALUE
        {
            get
            {
                if(UNIT_ID == Unit.Length)
                    return GetSize(plc_value);
                else if(UNIT_ID == Unit.Speed)
                    return GetSize(plc_value);
                else
                    return plc_value;
            }
            set
            {
                double new_value = value;

                if (UNIT_ID == Unit.Length)
                    SetSize(new_value, out plc_value);
                else if (UNIT_ID == Unit.Speed)
                    SetSize(new_value, out plc_value);

                if (value > max)
                    plc_value = max;
                else if (value < min)
                    plc_value = min;
            }
        }

        public double store_value = 0.0;
        public double STORE_VALUE
        {
            get { return (UNIT_ID == Unit.Length || UNIT_ID == Unit.Speed) ? GetSize(store_value) : store_value; }
        }
        public double min = 0.0;
        public double MIN
        {
            get { return (UNIT_ID == Unit.Length || UNIT_ID == Unit.Speed) ? GetSize(min): min; }
        }
        public double max = 0.0;
        public double MAX
        {
            get { return (UNIT_ID == Unit.Length || UNIT_ID == Unit.Speed) ? GetSize(max) : max; }
        }

        public string ADDRESS { get; set; } = string.Empty;
        public string ADDRESS2 { get; set; } = string.Empty;
        public string UNIT {
            get
            {
                if (UNIT_ID == Unit.Length)
                    return METRIC ? "mm" : "inch";
                else if (UNIT_ID == Unit.Speed)
                    return METRIC ? "mm/min" : "inch/min";
                else
                    return "N";
            }
        }
        public Unit UNIT_ID { get; set; } = Unit.Length;
    }

    public class DbParameters : DbBase
    {
        //--------------------------------------------------------------------------------------
        public DbParameters(DbBase db)
            : base(db)
        {
        }

        public void Read(DataMap<ParamData> parameters)
        {
            string select = "select PARAM_ID, PARAM_NAME, MIN_VALUE, MAX_VALUE, PARAM_VALUE, ADDRESS, ADDRESS2, UNIT_ID, UNIT_OF_MEASURE.NAME " +
                "from SETUP_PARAM inner join UNIT_OF_MEASURE on SETUP_PARAM.UNIT_ID = UNIT_OF_MEASURE.ID order by PARAM_ID";

            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                parameters.Clear();

                while (rdr.Read())
                {
                    ParamData data = new ParamData();
                    int i = 0;
                    data.ID = ReadInt32(rdr, i++);
                    data.NAME = ReadString(rdr, i++);
                    data.min = ReadDecimal(rdr, i++);
                    data.max = ReadDecimal(rdr, i++);
                    data.store_value = ReadDecimal(rdr, i++);
                    data.ADDRESS = ReadString(rdr, i++);
                    data.ADDRESS2 = ReadString(rdr, i++);
                    data.UNIT_ID = (ParamData.Unit)ReadInt32(rdr, i++);
                    //data.UNIT = ReadString(rdr, i++);

                    data.STATUS = EntityStatus.Set;
                    parameters.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }

            foreach (var p in parameters.DATA)
            {
                p.plc_value = ReadParameterPLC(p);
            }

            Logger.Write("Parameter [{0}] entries loaded", parameters.Count);
        }

        private void Update(ParamData data)
        {
            string str = String.Format("update SETUP_PARAM set PARAM_NAME='{0}', PARAM_VALUE={1} where PARAM_ID={2}",
                 data.NAME, data.plc_value, data.ID);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        private void Save(ParamData data)
        {
            switch (data.STATUS)
            {
                case EntityStatus.Upd:
                    Update(data);
                    WriteParameterPLC(data);
                    break;
                default:
                    break; // do nothing
            }

            data.STATUS = EntityStatus.Set;
        }

        public void Save(DataMap<ParamData> parameters)
        {
            try
            {
                OpenConnection();
                BeginTransaction();

                foreach (var p in parameters.DATA)
                {
                    Save(p);
                }

                CommitTransaction();
            }
            catch (Exception)
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        private static double ReadParameterPLC(ParamData p)
        {
            double res = 0.0;

            try
            {
                if (p.UNIT_ID == ParamData.Unit.Length)
                    return Global.PLC.ReadDouble(p.ADDRESS);
                else if (p.UNIT_ID == ParamData.Unit.Speed)
                {
                    double dd = Global.PLC.ReadDouble(p.ADDRESS, 1000);
                    return dd;
                }
                else if (p.UNIT_ID == ParamData.Unit.Count)
                    return Global.PLC.ReadInteger(p.ADDRESS);
            }
            catch (Exception ex)
            {
                Logger.Write(ex.Message);
            }

            return res;
        }

        private static bool WriteParameterPLC(ParamData p)
        {
            bool res = false;

            if (p.UNIT_ID == ParamData.Unit.Length)
                Global.PLC.WriteDouble(p.ADDRESS, p.plc_value);
            else if (p.UNIT_ID == ParamData.Unit.Speed)
                Global.PLC.WriteDouble(p.ADDRESS, p.plc_value, 1000);
            else if (p.UNIT_ID == ParamData.Unit.Count)
                Global.PLC.SetDevice(p.ADDRESS, (int)p.plc_value);

            if (res && p.ADDRESS2 != string.Empty)
                res = Global.PLC.SetDevice(p.ADDRESS2, 1);

            return res;
        }
    }
}

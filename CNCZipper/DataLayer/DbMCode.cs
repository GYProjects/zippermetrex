﻿using System;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class MCode : Entity
    {
    }

    class DbMCode : DbEntityBase<MCode>
    {
        //--------------------------------------------------------------------------------------
        public DbMCode(DbBase db)
            : base(db, "MCODE")
        {
        }

        public sealed override void Read(DataMap<MCode> mcodes)
        {
            string select = "select ID, NAME from MCODE order by ID";
            IDbCommand cmd = GetDbCommand(select);

            IDataReader rdr = null;
            try
            {
                OpenConnection();
                rdr = cmd.ExecuteReader();

                mcodes.Clear();

                while (rdr.Read())
                {
                    var data = new MCode();

                    data.ID = ReadInt32(rdr, 0);
                    data.NAME = ReadString(rdr, 1);
                    data.STATUS = EntityStatus.Set;
                    mcodes.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
                CloseConnection();
            }

            Logger.Write("MCode [{0}] entries loaded", mcodes.Count);
        }

        protected sealed override void Update(MCode data)
        {
            string str = String.Format("update MCODE set NAME='{0}' where ID={1}",
                 data.NAME, data.ID);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        protected sealed override void Insert(MCode data)
        {
            string str = String.Format("insert into MCODE(ID, NAME) values({0}, '{1}')",
                data.ID, data.NAME);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }
    }
}
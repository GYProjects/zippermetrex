﻿using System;
using System.ComponentModel;
using System.Data;
using CNCLib;

namespace CNCZipper.DataLayer
{
    public class ZippCycleDtl : Entity
    {
        public int CYCLE_ID { get; set; } = 0;
        public int STEP { get { return ID; } set { ID = value; } }

        public double w = 0.0;
        public double W
        {
            get { return GetSize(w); }
            set { SetSize(value, out w); }
        }

        public int L { get; set; } = 0;
        public int SPEED { get; set; } = 0;
        public int MCODE { get; set; } = 0;

        public string MCODE_DESC { get { return MCODE == 0 ? "" : Options.mcodes.Get(MCODE).ID_NAME; } }
    }

    public abstract class DbZippCycleDtl : DbBase
    {
        protected abstract string GetTableName();
        //--------------------------------------------------------------------------------------
        public DbZippCycleDtl(DbBase db)
            : base(db)
        {
        }

        public void Read(int id, DataMap<ZippCycleDtl> steps)
        {
            string str = String.Format("select STEP, NAME, L, W, SPEED, MCODE from {0} where CYCLE_ID={1} order by STEP",
                GetTableName(), id);

            IDbCommand cmd = GetDbCommand(str);

            IDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
                steps.Clear();

                while (rdr.Read())
                {
                    int i = 0;
                    var data = new ZippCycleDtl();
                    data.CYCLE_ID = id;
                    data.STEP = ReadInt32(rdr, i++);
                    data.NAME = ReadString(rdr, i++);
                    data.L = ReadInt32(rdr, i++);
                    data.w = ReadDecimal(rdr, i++);
                    data.SPEED = ReadInt32(rdr, i++);
                    data.MCODE = ReadInt32(rdr, i++);
                    data.STATUS = EntityStatus.Set;
                    steps.Add(data);
                }
            }
            finally
            {
                CloseReader(rdr);
            }
        }

        public void DeleteAll(int id)
        {
            string str = String.Format("delete from {0} where CYCLE_ID={1}", GetTableName(), id);
            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();
        }

        public void Insert(ZippCycleDtl data)
        {
            string str = String.Format("insert into {0} (CYCLE_ID, STEP, NAME, L, W, SPEED, MCODE) values({1}, {2}, '{3}', {4}, {5}, {6}, {7})",
                 GetTableName(), data.CYCLE_ID, data.STEP, data.NAME, data.L, data.w, data.SPEED, data.MCODE);

            IDbCommand cmd = GetDbCommand(str);
            cmd.ExecuteNonQuery();

            data.STATUS = EntityStatus.Set;
        }


    }
    public class DbFullZippCycleDtl : DbZippCycleDtl
    {
        protected override string GetTableName()
        {
            return "FULL_CYCLE_DTL";
        }

        //--------------------------------------------------------------------------------------
        public DbFullZippCycleDtl(DbBase db)
            : base(db)
        {

        }

    }

    public class DbRollZippCycleDtl : DbZippCycleDtl
    {
        protected override string GetTableName()
        {
            return "ROLL_CYCLE_DTL";
        }

        //--------------------------------------------------------------------------------------
        public DbRollZippCycleDtl(DbBase db)
            : base(db)
        {

        }
    }

}

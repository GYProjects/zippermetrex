﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace CNCZipper
{
    using CNCLib;
    using DataLayer;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer _timer = new DispatcherTimer();

        private ImageSource imageOn = null;
        private ImageSource imageOff = null;
        private ImageSource imageBack = null;
        private ImageSource imageClose = null;
        private ImageSource imageError = null;

        private Image _image_close = new Image();

        private ctrlMenu _menu;
        private ctrlParameters _parameters = new ctrlParameters();
        private ctrlManualMode _manual = new ctrlManualMode();
        private ctrlAssemblies _assemblies = new ctrlAssemblies();
        private ctrlStrut _struts = new ctrlStrut();
        private ctrlFixtures _fixtures = new ctrlFixtures();
        private ctrlZippCycles _cycles = new ctrlZippCycles();
        private ctrlKnurling _knurling = new ctrlKnurling();
        private ctrlAutoMode _auto = new ctrlAutoMode();
        private ctrlRolls _rolls = new ctrlRolls();
        


        private CNCLib.IControlData _data = null;

        //---------------------------------------------------------------------------------------
        public MainWindow()
        {
            InitializeComponent();

            imageOn = new BitmapImage(new Uri("pack://application:,,,/Images/open.png"));
            imageOff = new BitmapImage(new Uri("pack://application:,,,/Images/closed.png"));
            imageBack = new BitmapImage(new Uri("pack://application:,,,/Images/back.png"));
            imageClose = new BitmapImage(new Uri("pack://application:,,,/Images/exit.png"));
            imageError = new BitmapImage(new Uri("pack://application:,,,/Images/error.png"));

            btnClose.Content = _image_close;

            _menu = new ctrlMenu(MenuClick);
            Show(_menu);

            ShowPLC();

            int timer_tick = Int32.Parse(Global.settings["timer_tick"]);

            _timer.Tick += new EventHandler(UpdateWindow);
            _timer.Interval = new TimeSpan(0, 0, 0, 0, timer_tick); // 100 milliseconds
            _timer.Start();


            imgLogo.ToolTip = File.GetLastWriteTime(Application.ResourceAssembly.Location);
        }

        //---------------------------------------------------------------------------------------
        private void ShowPLC()
        {
            int res = 0;

            if(Global.PLC.IsConnected())
            {
                res = Global.PLC.CheckErrors();
                expErrors.Visibility = (res > 0) ? Visibility.Visible : Visibility.Hidden;
            }
            else
            {
                expErrors.Visibility = Visibility.Hidden;
            }

            if(res == 2)
            {
                txtErrors.Text = Global.PLC.GetErrors();
                if (!expErrors.IsExpanded)
                    expErrors.IsExpanded = true;
            }

            if (!Global.PLC.IsConnected())
                imgPLC.Source = imageOff;
            else
                imgPLC.Source = (res > 0)? imageError : imageOn;
        }

        //---------------------------------------------------------------------------------------
        private void UpdateWindow(object sender, EventArgs e)
        {
            if (_data != null)
            {
                _data.Update();
                ShowPLC();
            }
        }

        //---------------------------------------------------------------------------------------
        private void MenuClick(object sender, RoutedEventArgs e)
        {
            SetSubform((Button)e.Source);
        }

        //---------------------------------------------------------------------------------------
        private void Show<T>(T d) where T : UserControl, IControlData
        {
            if(_data != null)
                _data.SetActive(false);

            SubForm.Navigate(d);
            lblCaption.Content = d.GetTitle();
            _data = d;
            _data.SetActive(true);
            _image_close.Source = (_data == _menu) ? imageClose : imageBack;
        }

        //---------------------------------------------------------------------------------------
        private void SetSubform(Button btn)
        {
            if (btn == _menu.btnParameters)
                Show(_parameters);
            else if (btn == _menu.btnManualMode)
                Show(_manual);
            else if (btn == _menu.btnAssemblies)
                Show(_assemblies);
            else if (btn == _menu.btnStruts)
                Show(_struts);
            else if (btn == _menu.btnFixtures)
                Show(_fixtures);
            else if (btn == _menu.btnZippCycles)
                Show(_cycles);
            else if (btn == _menu.btnKnurling)
                Show(_knurling);
            else if (btn == _menu.btnAutoMode)
                Show(_auto);
            else if (btn == _menu.btnRolls)
                Show(_rolls);
            else if (btn == _menu.btnLogin)
                btn.Content = Login()? Global.GetText("TxtLogout") : Global.GetText("TxtLogin");
        }

        //---------------------------------------------------------------------------------------
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (_data ==_menu)
            {
                var dlg = new dlgConfirm(this, (string)Application.Current.Resources["TxtQuit"]);
                if (dlg.ShowDialog().Value)
                {
                    this.Close();
                }
            }
            else if (_data != null)
            {
                if (_data.IsModified())
                {
                    var dlg = new dlgConfirm(this, Global.GetText("TxtNotSavedQuit"));
                    if (dlg.ShowDialog().Value)
                        _data.UndoChanges();
                    else
                        return;
                }

                Show(_menu);
            }
        }

        //---------------------------------------------------------------------------------------
        private bool Login()
        {
            if (!Global.IsUserLogged())
            {
                UserData user = new UserData();

                //string prompt = Global.GetText("TxtLoginUserPrompt");
                //if (!dlgEntry.ReadData(Window.GetWindow(this), prompt, user, "NAME"))
                //    return false;

                user.NAME = string.Empty;
                string prompt = Global.GetText("TxtLoginPasswordPrompt");
                if (!dlgEntry.ReadData(GetWindow(this), prompt, user, "PASS"))
                    return false;
                else
                    return Global.Logon(user.NAME, user.PASS);
            }
            else
            {
                string prompt = String.Format(Global.GetText("TxtConfirmLogout"), Global.GetUser());
                dlgConfirm dlg = new dlgConfirm(this, prompt);
                if (dlg.ShowDialog().Value)
                {
                    Global.Logout();
                    return false;
                }
                else
                    return true;
            }
        }
    }
}

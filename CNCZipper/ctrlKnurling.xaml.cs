﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CNCLib;

namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlKnurling.xaml
    /// </summary>
    public partial class ctrlKnurling : UserControl, IControlData
    {
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public string GetTitle() { return Global.GetText("TxtKnurling"); }
        public void SetActive(bool flag) { }
        public void Update() {}

        public void UndoChanges()
        {
            if (Global.ReadKnurlingDB())
                SetModified(false);
        }

        public ctrlKnurling()
        {
            InitializeComponent();
            SetModified(false);
            gridKhurling.ItemsSource = Options.knurling.DATA;
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        void SetModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridKhurling.UnselectAll();
            gridKhurling.Items.Refresh();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Global.SaveKnurlingDB();
            Global.ReadKnurlingDB();

            SetModified(false);
        }

        private void gridKhurling_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridKhurling.CurrentCell);

            if (cent == null)
                return;

            bool done = false;

            if (cent.INDEX > 0 && cent.INDEX < 12)
                done = dlgEntry.ReadData(Window.GetWindow(this), "TxtKnurlingValuePrompt", cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetModified(true);
            }
        }

        private void GridDelete(object sender, RoutedEventArgs e)
        {
            var obj = gridKhurling.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Knurling))
            {
                var obj_to_delete = (Knurling)obj;

                if (obj_to_delete.STATUS == EntityStatus.New)
                    Options.knurling.Remove(obj_to_delete);
                else
                    obj_to_delete.STATUS = EntityStatus.Del;

                SetModified(true);
            }
        }

        private void GridCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridKhurling.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Knurling))
            {
                var obj_to_copy = (Knurling)obj;
                var obj_new = (Knurling)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                Options.knurling.AddNew(obj_new);
                SetModified(true);
            }
        }

        private void GridNew(object sender, RoutedEventArgs e)
        {
            var obj_new = new Knurling();
            Options.knurling.AddNew(obj_new);
            SetModified(true);
        }
    }
}

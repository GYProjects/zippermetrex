﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CNCLib;

namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlRolls.xaml
    /// </summary>
    public partial class ctrlRolls : UserControl, IControlData
    {
        public string GetTitle() { return Global.GetText("TxtRolls"); }
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public void SetActive(bool flag) {}
        public void Update() {}

        public void UndoChanges()
        {
            if (Global.ReadRollDB())
                SetModified(false);
        }

        public ctrlRolls()
        {
            InitializeComponent();
            SetModified(false);
            gridRolls.ItemsSource = Options.rolls.DATA;
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        void SetModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridRolls.UnselectAll();
            gridRolls.Items.Refresh();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Global.SaveRollDB();
            Global.ReadRollDB();

            SetModified(false);
        }

        private void gridRolls_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridRolls.CurrentCell);

            if (cent == null)
                return;

            bool done = false;

            if (cent.INDEX == 1 || cent.INDEX == 2)
                done = dlgEntry.ReadData(Window.GetWindow(this), "TxtRollValuePrompt", cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetModified(true);
            }
        }

        private void GridDelete(object sender, RoutedEventArgs e)
        {
            var obj = gridRolls.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Roll))
            {
                var obj_to_delete = (Roll)obj;

                if (obj_to_delete.STATUS == EntityStatus.New)
                    Options.rolls.Remove(obj_to_delete);
                else
                    obj_to_delete.STATUS = EntityStatus.Del;

                SetModified(true);
            }
        }

        private void GridCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridRolls.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Roll))
            {
                var obj_to_copy = (Roll)obj;
                var obj_new = (Roll)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                Options.rolls.AddNew(obj_new);
                SetModified(true);
            }
        }

        private void GridNew(object sender, RoutedEventArgs e)
        {
            var obj_new = new Roll();
            Options.rolls.AddNew(obj_new);
            SetModified(true);
        }
    }
}


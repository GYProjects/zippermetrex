﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace CNCZipper
{
    interface ILabel
    {
        void SetData(Object data);
        PaperSize GetPaperSize();
    }

    public class LabelPrint
    {
        private Form label_form;
        private Object[] label_data;
        private int page_index = 0;

        public LabelPrint(Form form, Object[] data)
        {
            label_form = form;
            label_data = data;
            var create = form.GetType().GetMethod("CreateControl", BindingFlags.Instance | BindingFlags.NonPublic);
            create.Invoke(label_form, new object[] { true });
        }

        public bool Print(bool show_dialog)
        {
            page_index = 0;
            var pd = new PrintDocument();
            pd.DefaultPageSettings.PrinterResolution = new PrinterSettings().DefaultPageSettings.PrinterResolution;

            pd.PrintPage += new PrintPageEventHandler(PrintPage);
            pd.DefaultPageSettings.PaperSize = ((ILabel)label_form).GetPaperSize();
            pd.BeginPrint += new System.Drawing.Printing.PrintEventHandler(BeginPrint);
            pd.PrinterSettings.PrinterName = new PrinterSettings().PrinterName;

            if (show_dialog)
            {
                var dialog = new PrintPreviewDialog();
                dialog.UseAntiAlias = true;
                dialog.Document = pd;
                dialog.ShowDialog();
                return false;
            }
            else
            {
                pd.Print();
                return true;
            }
        }

        public void BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            page_index = 0;
        }

        private void PrintPage(System.Object sender, PrintPageEventArgs e)
        {
            ((ILabel)label_form).SetData(label_data[page_index++]);

            var rec = label_form.ClientRectangle;
            var image = new Bitmap(rec.Width, rec.Height);
            label_form.DrawToBitmap(image, rec);

            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.CompositingQuality = CompositingQuality.GammaCorrected;
            e.Graphics.CompositingMode = CompositingMode.SourceCopy;

            e.Graphics.DrawImage(image, new Rectangle(0, 0, rec.Width/2, rec.Height/2));

            image.Dispose();
            e.HasMorePages = page_index < label_data.Length;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CNCLib;
namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlFixtures.xaml
    /// </summary>
    public partial class ctrlFixtures : UserControl, IControlData
    {
        public string GetTitle() { return Global.GetText("TxtFixtures"); }
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public void SetActive(bool flag) {}
        public void Update() {}

        public void UndoChanges()
        {
            if (Global.ReadFixtureDB())
                SetModified(false);
        }

        public ctrlFixtures()
        {
            InitializeComponent();
            SetModified(false);
            gridFixtures.ItemsSource = Options.fixtures.DATA;
        }
        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        void SetModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridFixtures.UnselectAll();
            gridFixtures.Items.Refresh();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Global.SaveFixtureDB();
            Global.ReadFixtureDB();

            SetModified(false);
        }

        private void gridFixtures_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridFixtures.CurrentCell);
            if (cent == null)
                return;

            bool done = false;

            if(cent.INDEX > 0 && cent.INDEX < 12)
                done = dlgEntry.ReadData(Window.GetWindow(this), "TxtFixtureValuePrompt", cent);
  
            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetModified(true);
            }
        }

        private void GridDelete(object sender, RoutedEventArgs e)
        {
            var obj = gridFixtures.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Fixture))
            {
                var obj_to_delete = (Fixture)obj;

                if (obj_to_delete.STATUS == EntityStatus.New)
                    Options.fixtures.Remove(obj_to_delete);
                else
                    obj_to_delete.STATUS = EntityStatus.Del;

                SetModified(true);
            }
        }

        private void GridCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridFixtures.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Fixture))
            {
                var obj_to_copy = (Fixture)obj;
                var obj_new = (Fixture)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                Options.fixtures.AddNew(obj_new);
                SetModified(true);
            }
        }

        private void GridNew(object sender, RoutedEventArgs e)
        {
            var obj_new = new Fixture();
            Options.fixtures.AddNew(obj_new);
            SetModified(true);
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CNCLib;
namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlParameters.xaml
    /// </summary>
    public partial class ctrlParameters : UserControl, IControlData
    {
        public string GetTitle() { return Global.GetText("TxtParameters"); }
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public void SetActive(bool flag) {}
        public void Update() {}

        public void UndoChanges()
        {
            if (Global.ReadParamDB())
                SetModified(false);
        }

        public ctrlParameters()
        {
            InitializeComponent();
            SetModified(false);
            gridParams.ItemsSource = Options.parameters.DATA;
        }

        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        void SetModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridParams.UnselectAll();
            gridParams.Items.Refresh();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Global.SaveParamDB();
            Global.PLC.SetDevice("M460", 1);
            Global.PLC.SetDevice("M461", 1);
            Global.PLC.SetDevice("M462", 1);
            Global.PLC.SetDevice("M463", 1);
            Global.PLC.SetDevice("M464", 1);
            Global.PLC.SetDevice("M465", 1);
            Global.PLC.SetDevice("M466", 1);
            Global.PLC.SetDevice("M467", 1);
            Global.PLC.SetDevice("M468", 1);
            Global.PLC.SetDevice("M469", 1);
            Global.ReadParamDB();
           
            SetModified(false);
        }

        private void gridParams_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridParams.CurrentCell);

            if (cent == null)
                return;

            bool done = false;

            if (cent.INDEX == 1 || cent.INDEX == 2)
                done = dlgEntry.ReadData(Window.GetWindow(this), "TxtParamValuePrompt", cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetModified(true);
            }
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M8", 1);
        }

        private void btnHomeHead_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M15", 1);
        }
    }
}

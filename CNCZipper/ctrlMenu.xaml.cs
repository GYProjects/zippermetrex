﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CNCZipper
{
    /// <summary>
    /// Interaction logic for ctrlMenu.xaml
    /// </summary>
    public partial class ctrlMenu : UserControl, CNCLib.IControlData
    {
        public string GetTitle() { return Global.GetText("TxtMainMenu"); }
        public bool IsModified() { return false; }
        public void UndoChanges() {}

        public void SetActive(bool flag)
        {
            if (flag)
                Global.PLC.ResetErrors();
        }

        public void Update()
        {
            Visibility v = Global.IsUserLogged()? Visibility.Visible : Visibility.Hidden;

            if (btnAssemblies.Visibility != v)
            {
                btnAssemblies.Visibility = v;
                btnFixtures.Visibility = v;
                btnRolls.Visibility = v;
                //btnManualMode.Visibility = v;
                btnParameters.Visibility = v;
                btnStruts.Visibility = v;
                btnZippCycles.Visibility = v;
                btnKnurling.Visibility = v;
            }
        }

        public ctrlMenu(RoutedEventHandler handler)
        {
            InitializeComponent();
            MenuClick += handler;
            Update();
        }

        private event RoutedEventHandler MenuClick;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (MenuClick != null)
            {
                MenuClick(this, e);
            }
        }
    }
}

﻿using System.Windows.Controls;
using CNCLib;

namespace CNCZipper
{
    public class CellEntity
    {
        public Entity OBJECT { get; set; } = null;
        public string HEADER { get; set; } = string.Empty;
        public string PROPERTY { get; set; } = string.Empty;
        public int INDEX { get; set; } = 0;
        public int ID { get { return OBJECT.ID; } }

        public EntityStatus STATUS
        {
            get { return OBJECT.STATUS; }
            set { OBJECT.STATUS = value; }
        }
    }

    public class Utils
    {
        public static CellEntity GetCellEntity(DataGridCellInfo cell)
        {
            if (cell.Column == null)
                return null;

            var obj = (Entity)cell.Item;

            if (obj == null)
                return null;

            var bind = (System.Windows.Data.Binding)cell.Column.ClipboardContentBinding;

            if (bind == null)
                return null;

            var ce = new CellEntity();
            ce.OBJECT = obj;
            ce.PROPERTY = bind.Path.Path;
            ce.HEADER = cell.Column.Header.ToString();
            ce.INDEX = cell.Column.DisplayIndex;

            return ce;
        }
    }
}

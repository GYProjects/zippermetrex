﻿using System;
using System.Text;
using System.ComponentModel;
using CNCLib;

namespace CNCZipper
{
    using DataLayer;
    public class ZipperPLC : Controller //EPLC
    {
        public ZipperPLC(bool emulate) : base(emulate)
        {
        }

        public int[] NewErrors = new int[] { 0, 0, 0, 0 };
        public int[] OldErrors = new int[] { 0, 0, 0, 0 };

        public bool[] MRegister = new bool[16];
        public bool[] SW3 = new bool[16];
        public bool[] SW5 = new bool[5];
        public double[] PRegister = new double[11];
        public double[] OffsetRegister = new double[8];

        //--------------------------------------------------------------------------------------
        public bool OverrideSpeed(int percent)
        {
            return SetDevice("D98", percent);
        }

        //--------------------------------------------------------------------------------------
        public int CheckErrors()
        {
            bool has_errors = false;
            bool new_errors = false;

            for (int i = 0; i < NewErrors.Length; i++)
            {
                if (NewErrors[i] > 0)
                    has_errors = true;

                if (NewErrors[i] != OldErrors[i])
                    new_errors = true;

                OldErrors[i] = NewErrors[i];
            }

            return has_errors ? (new_errors? 2:1) : 0;
        }

        //--------------------------------------------------------------------------------------
        public void ResetErrors()
        {
            for (int i = 0; i < NewErrors.Length; i++)
            {
                OldErrors[i] = NewErrors[i] = 0;
            }
        }
        //--------------------------------------------------------------------------------------
        public string GetErrors()
        {
            StringBuilder bld = new StringBuilder();
            for(int i = 0; i < NewErrors.Length; i++)
            {
                if (NewErrors[i] == 0)
                    continue;

                for (int j = 0; j < 16; j++)
                {
                    int mask = 1 << j;
                    if ((mask & NewErrors[i]) != 0)
                    {
                        int err_number = i * 16 + j;
                        var msg = Options.msgs.Find(err_number);

                        if(msg != null)
                        {
                            int warn = 0;
                            GetDevice(msg.WARNING, out warn);
                            int fatal = 0;

                            if (msg.FATAL == msg.WARNING)
                                fatal = warn;
                            else
                                GetDevice(msg.FATAL, out fatal);

                            bld.Append(string.Format("{0:00}. {1} [{2}-{3}]", err_number, msg.NAME, warn, fatal));
                        }
                        else
                        {
                            bld.Append(string.Format("{0}. Unknown Error", err_number));
                        }

                        bld.Append(Environment.NewLine);
                    }
                }
                NewErrors[i] = 0;
            }
            return bld.ToString();
        }

        //--------------------------------------------------------------------------------------
        public bool Toggle_K8Y20(int val)
        {
            int value = 0;
            if (GetDevice("K8Y20", out value))
            {
                value ^= (1 << val);
                return SetDevice("K8Y20", value);
            }
            else
                return false;
        }

        //--------------------------------------------------------------------------------------
        public bool LoadCycle(BindingList<ZippCycleDtl> steps, double length)
        {
            int[] arr = new int[steps.Count * 5];

            int idx = 0;
            foreach(var step in steps)
            {
                SetDouble((step.w + step.L * length), ref arr[idx], ref arr[idx + 1]);
                SetDouble(step.SPEED, 1000, ref arr[idx + 2], ref arr[idx + 3]);
                arr[idx + 4] = step.MCODE;
                idx += 5;
            }

            return WriteDeviceBlock("D1000", arr) && SetDevice("D8", steps.Count);
        }

        //--------------------------------------------------------------------------------------
        public bool LoadAssembly(Assembly a)
        {
            int[] arr = new int[46];

            Fixture fx = Options.fixtures.Get(a.FIXTURE_ID);
            Strut st = Options.struts.Get(a.STRUT_ID);
            Knurling kn = Options.knurling.Get(a.KNURLING_SET_ID);
            Roll rl = Options.rolls.Get(a.ROLL_SET_ID);


            SetDouble(fx.height, ref arr[0], ref arr[1]);
            SetDouble(a.bot_roll_height, ref arr[2], ref arr[3]);
            SetDouble(a.top_roll_height, ref arr[4], ref arr[5]);
            SetDouble(st.width, ref arr[6], ref arr[7]);
            arr[8] = a.front_roll_psi; arr[9] = 0;

            double STAT_TABLE = a.stat_table + fx.stat_table_width;
            double MOVE_TABLE = a.move_table + fx.move_table_width;

            SetDouble(STAT_TABLE, ref arr[10], ref arr[11]);
            SetDouble(MOVE_TABLE, ref arr[12], ref arr[13]);
            SetDouble(a.front_offset_top, ref arr[14], ref arr[15]);
            SetDouble(a.back_offset_top, ref arr[16], ref arr[17]);
            SetDouble(a.front_offset_bot, ref arr[18], ref arr[19]);
            SetDouble(a.back_offset_bot, ref arr[20], ref arr[21]);
            arr[22] = 0; arr[23] = 0;
            arr[24] = a.CYCLE_ID; arr[25] = 0;
            SetDouble(a.top_knurling_vert, ref arr[26], ref arr[27]);
            SetDouble(a.bot_knurling_vert, ref arr[28], ref arr[29]);
            SetDouble(kn.z_correction, ref arr[30], ref arr[31]);
            SetDouble(rl.z_correction, ref arr[32], ref arr[33]);
            SetDouble(a.preroll_horz, ref arr[34], ref arr[35]);
            SetDouble(a.preroll_vert, ref arr[36], ref arr[37]);
            SetDouble(fx.move_table_height, ref arr[38], ref arr[39]);
            SetDouble(a.stat_table_unload, ref arr[40], ref arr[41]);
            SetDouble(a.move_table_unload, ref arr[42], ref arr[43]);
            SetDouble(fx.stat_table_height, ref arr[44], ref arr[45]);


            return this.SetDevice("D890", a.ID) && WriteDeviceBlock("D800", arr);
        }

        public void TempWriteBlock(string a, int[] d)
        {
            WriteDeviceBlock(a, d);
        }

        //--------------------------------------------------------------------------------------
        public bool ReadDeviceBlock_D100_32()
        {
            int[] arr = new int[32];

            bool res = ReadDeviceBlock("D100", arr);

            if(res)
            {
                for (int i = 0; i < NewErrors.Length; i++)
                    NewErrors[i] = arr[i];

                for (int i = 0; i < MRegister.Length; i++)
                    MRegister[i] = ((1 << i) & arr[4]) > 0;

                for (int i = 0; i < SW3.Length; i++)
                    SW3[i] = ((1 << i) & arr[5]) > 0;

                for (int i = 0; i < SW5.Length; i++)
                    SW5[i] = ((1 << i) & arr[8]) > 0;

                for (int i = 0; i < PRegister.Length; i++)
                    PRegister[i] = GetDouble(arr[2 * i + 10], arr[2 * i + 11]);
            }

            return res;
        }

        //--------------------------------------------------------------------------------------
        public bool ReadOffset_D840_16()
        {
            int[] arr = new int[OffsetRegister.Length * 2];

            bool res = ReadDeviceBlock("D840", arr);

            if (res)
            {
                for (int i = 0; i < OffsetRegister.Length; i++)
                {
                    OffsetRegister[i] = GetDouble(arr[2 * i], arr[2 * i + 1]);
                }
            }

            return res;
        }

        //--------------------------------------------------------------------------------------
        public bool StartMove(int button_tag)
        {
            switch (button_tag)
            { 
            case 20:
                return SetDevice("M178", 1);
            case 21:
                return SetDevice("M179", 1);
            default:
                int i = 1 << button_tag;
                return SetDevice("K5M180", i);
            }
        }
        //--------------------------------------------------------------------------------------
        public bool StopMove()
        {
            return SetDevice("K5M180", 0) && SetDevice("M178", 0) && SetDevice("M179", 0);
        }
    }
}

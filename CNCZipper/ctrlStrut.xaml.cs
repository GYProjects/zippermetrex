﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CNCLib;
namespace CNCZipper
{
    using DataLayer;

    /// <summary>
    /// Interaction logic for ctrlStrut.xaml
    /// </summary>
    public partial class ctrlStrut : UserControl, IControlData
    {
        public bool IsModified() { return btnSave.Visibility == Visibility.Visible; }
        public string GetTitle() { return Global.GetText("TxtStruts"); }
        public void SetActive(bool flag) {}
        public void Update() { }

        public void UndoChanges()
        {
            if (Global.ReadStrutDB())
                SetModified(false);
        }

        public ctrlStrut()
        {
            InitializeComponent();
            SetModified(false);
            gridStruts.ItemsSource = Options.struts.DATA;
        }
        private void btnUndo_Click(object sender, RoutedEventArgs e)
        {
            UndoChanges();
        }

        void SetModified(bool val)
        {
            btnSave.Visibility = btnUndo.Visibility = val ? Visibility.Visible : Visibility.Hidden;
            gridStruts.UnselectAll();
            gridStruts.Items.Refresh();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Global.SaveStrutDB();
            Global.ReadStrutDB();

            SetModified(false);
        }

        private void gridParams_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cent = Utils.GetCellEntity(gridStruts.CurrentCell);

            if (cent == null)
                return;

            bool done = false;

            if (cent.INDEX == 1 || cent.INDEX == 2)
                done = dlgEntry.ReadData(Window.GetWindow(this), "TxtStrutValuePrompt", cent);

            if (done)
            {
                if (cent.STATUS == EntityStatus.Set)
                    cent.STATUS = EntityStatus.Upd;

                SetModified(true);
            }
        }

        private void GridDelete(object sender, RoutedEventArgs e)
        {
            var obj = gridStruts.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Strut))
            {
                var obj_to_delete = (Strut)obj;

                if (obj_to_delete.STATUS == EntityStatus.New)
                    Options.struts.Remove(obj_to_delete);
                else
                    obj_to_delete.STATUS = EntityStatus.Del;

                SetModified(true);
            }
        }

        private void GridCopy(object sender, RoutedEventArgs e)
        {
            var obj = gridStruts.SelectedItem;
            if (obj != null && obj.GetType() == typeof(Strut))
            {
                var obj_to_copy = (Strut)obj;
                var obj_new = (Strut)obj_to_copy.Clone();
                obj_new.STATUS = EntityStatus.New;
                Options.struts.AddNew(obj_new);
                SetModified(true);
            }
        }

        private void GridNew(object sender, RoutedEventArgs e)
        {
            var obj_new = new Strut();
            Options.struts.AddNew(obj_new);
            SetModified(true);
        }
    }
}


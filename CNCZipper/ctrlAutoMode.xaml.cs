﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using CNCLib;

namespace CNCZipper
{
    using DataLayer;
    /// <summary>
    /// Interaction logic for ctrlAutoMode.xaml
    /// </summary>
    /// 
    public class ParamPair
    {
        public string NAME { get; set; } = string.Empty;
        public string VALUE { get; set; } = string.Empty;

        public ParamPair(string name, string value)
        {
            NAME = name; VALUE = value;
        }

        public EntityStatus STATUS { get; set; } = EntityStatus.Set;
    }

    public class DoubleParam
    {
        public string ADDRESS { get;} = string.Empty;
        public double value = 0.0;

        public DoubleParam(string addr, double val)
        {
            ADDRESS = addr; value = val;
        }

        public double VALUE
        {
            get { return Entity.GetSize(value); }
            set { Entity.SetSize(value, out this.value); }
        }
    }

    public class IntegerParam
    {
        public string ADDRESS { get;} = string.Empty;
        public int VALUE { get; set; } = 0;

        public IntegerParam(string address, int value)
        {
            ADDRESS = address; VALUE = value;
        }
    }


    public partial class ctrlAutoMode : UserControl, IControlData
    {
        private Brush ColorOn;
        private Brush ColorOff;

        private static string TXT_PAUSE = string.Empty;
        private static string TXT_CONTINUE = string.Empty;

        private IntegerParam param_counter = new IntegerParam("D888", 0);
        private DoubleParam param_length = new DoubleParam("D900", 0.0);

        private ClientEntry client_entry = new ClientEntry();

        frmLabel.Data label_data = null;

        public string GetTitle() { return Global.GetText("TxtAutoMode"); }
        public bool IsModified() { return false; }
        public void UndoChanges() {}

        //---------------------------------------------------------------------------------
        public void SetActive(bool active)
        {
            if(!active)
            {
                Global.PLC.SetDevice("M1", 0);
                Global.PLC.SetDevice("M56", 0);
            }
            else
            {
                if (Global.assembly != null)
                    cmbAssembly.SelectedItem = Global.assembly;

                TXT_PAUSE = Global.GetText("TxtPause");
                TXT_CONTINUE = Global.GetText("TxtContinue"); 

                try
                {
                    param_length.value = Global.PLC.ReadDouble("D900");
                    lblLengthValue.Content = param_length.VALUE.ToString(); // string.Format("{0:0.00}", param_length.VALUE2);
                }
                catch
                {
                    lblLengthValue.Content = "????";
                }
            }
        }

        //---------------------------------------------------------------------------------
        public void Update()
        {
            Global.PLC.ReadDeviceBlock_D100_32();

            if (Global.PLC.MRegister[6])
            {
                btnPauseContinue.Content = Global.PLC.MRegister[14] ? TXT_CONTINUE : TXT_PAUSE;

                if (btnPauseContinue.Visibility == Visibility.Hidden)
                    btnPauseContinue.Visibility = Visibility.Visible;
            }
            else 
            {
                if (btnPauseContinue.Visibility == Visibility.Visible)
                    btnPauseContinue.Visibility = Visibility.Hidden;
            }

            if (Global.PLC.MRegister[5])
            {
                try
                {
                    int counter = param_counter.VALUE;
                    param_counter.VALUE = Global.PLC.ReadInteger(param_counter.ADDRESS);
                    lblCountValue.Content = string.Format("{0:0000}", param_counter.VALUE);

                    if (counter < param_counter.VALUE)
                        PrintLabel(false);
                }
                catch
                {
                    lblCountValue.Content = "????";
                }

                Global.PLC.SetDevice("M55", 0);
            }

            bool load_full_cycle = Global.PLC.MRegister[8];
            bool load_roll_cycle = Global.PLC.MRegister[10];

            if (load_full_cycle || load_roll_cycle)
            {
                Assembly a = (Assembly)cmbAssembly.SelectedItem;
                if (a != null)
                {
                    var cycle = Options.cycles.Find(a.CYCLE_ID);

                    if(load_full_cycle)
                    {
                        Global.PLC.MRegister[8] = false;
                        Global.PLC.LoadCycle(cycle.full_steps.DATA, param_length.value); // writes array into D1000 and Size into D8
                        Global.PLC.SetDevice("M58", 0);

                    }
                    else if (load_roll_cycle)
                    {
                        Global.PLC.MRegister[10] = false;
                        Global.PLC.SetDevice("M92", 0);
                        Global.PLC.LoadCycle(cycle.roll_steps.DATA, param_length.value); // writes array into D1000 and Size into D8
                        Global.PLC.SetDevice("M60", 0);
                    }
                }
            }

            ledCurtain.Fill = Global.PLC.SW3[15] ? ColorOn : ColorOff;

            if (Global.PLC.MRegister[11])
            {
                client_entry.st = DateTime.Now;
                client_entry.employee = Int32.Parse(lblEmployeeValue.Content.ToString());
                client_entry.job_id = Int32.Parse(lblJobValue.Content.ToString()); //lblJobValue.Content.ToString();
                
                Global.PLC.SetDevice("M61", 0);
            }

            if (Global.PLC.MRegister[12])
            {
                client_entry.ft = DateTime.Now;
                int strutdelay = 0;
                Global.PLC.GetDevice("D2", out strutdelay);
                client_entry.str = strutdelay;
                Global.SaveForClient(client_entry);
                Global.PLC.SetDevice("M62", 0);
            }
        }

        //---------------------------------------------------------------------------------
        public ctrlAutoMode()
        {
            InitializeComponent();
            cmbAssembly.ItemsSource = Options.assemblies.DATA;
            cmbAssembly.DisplayMemberPath = "ID_NAME";
            cmbAssembly.SelectedValuePath = "ID";

            lblCountValue.ToolTip = Global.GetText("TxtResetCounter");
            lblLengthValue.ToolTip = Global.GetText("TxtModifyLength");

            ColorOn = (Brush)FindResource("LEDColorON");
            ColorOff = (Brush)FindResource("LEDColorOFF");
        }

        //---------------------------------------------------------------------------------
        private BindingList<ParamPair> GetParameters(Assembly a)
        {
            Fixture fx = Options.fixtures.Get(a.FIXTURE_ID);

            return new BindingList<ParamPair>()
            {
                new ParamPair(Global.GetText("TxtCycle"), a.CYCLE),
                new ParamPair(Global.GetText("TxtStrutID"), a.STRUT),
                new ParamPair(Global.GetText("TxtKnurlingSet"), a.KNURLING_SET),
                new ParamPair(Global.GetText("TxtRollSet"), a.ROLL_SET),
                new ParamPair(Global.GetText("TxtFixtureID"), a.FIXTURE),
                new ParamPair(Global.GetText("TxtFrontRollPSI"), a.FRONT_ROLL_PSI.ToString()),
                new ParamPair(Global.GetText("TxtBackRollPSI"), a.BACK_ROLL_PSI.ToString()),
                new ParamPair(Global.GetText("TxtTopLeftKnurlingPSI"), a.TOP_LEFT_KNURLING_PSI.ToString()),
                new ParamPair(Global.GetText("TxtTopRightKnurlingPSI"), a.TOP_RIGHT_KNURLING_PSI.ToString()),
                new ParamPair(Global.GetText("TxtBottomLeftKnurlingPSI"), a.BOTTOM_LEFT_KNURLING_PSI.ToString()),
                new ParamPair(Global.GetText("TxtBottomRightKnurlingPSI"), a.BOTTOM_RIGHT_KNURLING_PSI.ToString()),
                new ParamPair("FX: " + Global.GetText("TxtStatTable"), fx.STAT_TABLE_ID),
                new ParamPair("FX: " + Global.GetText("TxtFrontStatID"), fx.FRONT_STAT_ID),
                new ParamPair("FX: " + Global.GetText("TxtBackStatID"), fx.BACK_STAT_ID),

                new ParamPair("FX: " + Global.GetText("TxtMoveTable"), fx.MOVE_TABLE_ID),
                new ParamPair("FX: " + Global.GetText("TxtFrontMoveID"), fx.FRONT_MOVE_ID),
                new ParamPair("FX: " + Global.GetText("TxtBackMoveID"), fx.BACK_MOVE_ID)
            };
        }

        //---------------------------------------------------------------------------------
        private void cmbAssembly_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var a = (Assembly)cmbAssembly.SelectedItem;

            if (a != null)
            {
                Global.assembly = a;
                Global.PLC.LoadAssembly(a);
                gridParameters.ItemsSource = GetParameters(a);
                lblDrawing.Content = a.NAME;
                imgDrawing.Source = a.IMAGE;

                client_entry.profileID = a.NAME;
            }
        }

        //---------------------------------------------------------------------------------
        private void SpeedScrollbar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int speed_percent = (int)e.NewValue;

            if (lblSpeedPercent != null)
            {
                lblSpeedPercent.Content = string.Format(Global.GetText("TxtMotorSpeed"), speed_percent);
            }

            Global.PLC.OverrideSpeed(speed_percent);
        }

        //---------------------------------------------------------------------------------
        private void lblCounter_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            dlgConfirm dlg = new dlgConfirm(Window.GetWindow(this), Global.GetText("TxtResetCycleCounter"));
            if (dlg.ShowDialog().Value)
            {
                if(Global.PLC.SetDevice("D888", 0))
                {
                    param_counter.VALUE = 0;
                    lblCountValue.Content = string.Format("{0:0000}", param_counter.VALUE);
                }
            }
        }

        //---------------------------------------------------------------------------------
        private void lblLengthValue_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            bool done = dlgEntry.ReadData(Window.GetWindow(this), Global.GetText("TxtEnterLengthPrompt"), param_length, "VALUE");

            if (done)
            {
                try
                {
                    if(Global.PLC.WriteDouble("D900", param_length.value))
                        param_length.value = Global.PLC.ReadDouble("D900");

                    lblLengthValue.Content = param_length.VALUE.ToString(); //string.Format("{0:0.000}", param_length.VALUE);
                }
                catch
                {
                    lblLengthValue.Content = "???";
                }
            }
        }

        //---------------------------------------------------------------------------------
        private void lblJobValue_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dlgEntry.ReadData(Window.GetWindow(this), "Please Select Job", Global.GetClientJobs(), client_entry, "job_id"))
            {
                client_entry.job = Global.GetClientJobName(client_entry.job_id);
                lblJobValue.Content = client_entry.job;
            }
        }

        //---------------------------------------------------------------------------------
        private void lblEmployee_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dlgEntry.ReadData(Window.GetWindow(this), "Please Enter Emploee ID", client_entry, "employee"))
                lblEmployeeValue.Content = client_entry.employee;
        }

        //---------------------------------------------------------------------------------
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            if (!Global.PLC.MRegister[6])
                Global.PLC.SetDevice("M9", 1);
        }

        //---------------------------------------------------------------------------------
        private void btnRoll_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M60", 1);
        }

        //---------------------------------------------------------------------------------
        private void btnPedal_Click(object sender, RoutedEventArgs e)
        {
            Global.PLC.SetDevice("M7", 1);
        }

        //---------------------------------------------------------------------------------
        private void btnPauseContinue_Click(object sender, RoutedEventArgs e)
        {
            if(Global.PLC.MRegister[6])
                Global.PLC.SetDevice("M74", 1);
        }

        private void btnFile_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.InitialDirectory = Global.settings["batch_dir"];
            dialog.Title = "Select a csv file";
            dialog.Filter = "CSV Files (*.csv)|*.csv";

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] lines = System.IO.File.ReadAllLines(dialog.FileName);

                label_data = frmLabel.Data.Build(lines);

                if(label_data != null)
                {
                    cmbAssembly.SelectedValue = label_data.id;

                    param_length.VALUE = label_data.length;
                    Global.PLC.WriteDouble("D900", param_length.value);
                    lblLengthValue.Content = param_length.VALUE.ToString(); //string.Format("{0:0.000}", param_length.VALUE);

                    lblFileCount.Content = string.Format("{0:000}", label_data.quantity);

                    PrintLabel(true);
                }
            }
        }

        //---------------------------------------------------------------------------------
        private void PrintLabel(bool show_dialog)
        {
            if (label_data != null)
            {
                var prt = new LabelPrint(new frmLabel(), new Object[] { label_data });
                try
                {
                    if(prt.Print(show_dialog))
                    {
                        if (++label_data.counter > label_data.quantity)
                        {
                            label_data = null;
                            lblFileCount.Content = "000";
                        }
                        else
                            lblFileCount.Content = string.Format("{0:000}", label_data.quantity - label_data.counter);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}

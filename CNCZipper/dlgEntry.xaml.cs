﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
//using CNC_WPF.Utils;
using System.Windows.Controls;
using System.Windows.Media;
using CNCLib;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace CNCZipper
{
    /// <summary>
    /// Interaction logic for dlgEntry.xaml
    /// </summary>
    public partial class dlgEntry : Window
    {
        private Object entry_obj = null;
        private PropertyInfo entry_prop = null;

        TextBox txtValue = null;
        ComboBox cmbValue = null;
        PasswordBox pwdValue = null;

        private void InitCommon(Window owner, string prompt)
        {
            InitializeComponent();

            if (owner != null)
            {
                this.Owner = owner;
                lblPrompt.Content = prompt;
                this.Top = owner.Top + 20;// + 
                this.Left = owner.Left + owner.Width / 2 - this.Width / 2;
            }
        }

        private void CmbValue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            entry_prop.SetValue(entry_obj, ((Entity)cmbValue.SelectedItem).ID, null);
            this.DialogResult = true;
        }

        public dlgEntry()
        {
            InitCommon(null, null);
        }

        public static bool ReadData(Window owner, string prompt, Object obj, string prop_name)
        {
            dlgEntry entry = new dlgEntry(owner, prompt, obj, prop_name);
            return entry.ShowDialog().Value;
        }

        public static bool ReadData(Window owner, string prompt_key, CellEntity cent)
        {
            string prompt = string.Format(Global.GetText(prompt_key), cent.ID, cent.HEADER);
            dlgEntry entry = new dlgEntry(owner, prompt, cent.OBJECT, cent.PROPERTY);
            return entry.ShowDialog().Value;
        }

        public static bool ReadData(Window owner, string prompt, List<Entity> options, Object obj, string prop_name)
        {
            dlgEntry entry = new dlgEntry(owner, prompt, options, obj, prop_name);
            return entry.ShowDialog().Value;
        }

        public static bool ReadData(Window owner, string prompt_key, List<Entity> options, CellEntity cent, string prop_name)
        {
            string prompt = string.Format(Global.GetText(prompt_key), cent.ID, cent.HEADER);
            dlgEntry entry = new dlgEntry(owner, prompt, options, cent.OBJECT, prop_name);
            return entry.ShowDialog().Value;
        }

        public dlgEntry(Window owner, string prompt, Object obj, string prop_name)
        {
            InitCommon(owner, prompt);

            entry_obj = obj;
            entry_prop = obj.GetType().GetProperty(prop_name);

            Control ctrl = null;

            if (prop_name.Equals("PASS"))
            {
                pwdValue = new PasswordBox();
                ctrl = pwdValue;
                pwdValue.KeyDown += pwdValue_KeyDown;

                Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
                {
                    pwdValue.Focus();
                }));
            }
            else
            {
                txtValue = new TextBox();
                ctrl = txtValue;
                txtValue.KeyDown += txtValue_KeyDown;
                txtValue.Text = entry_prop.GetValue(entry_obj, null).ToString();

                if (entry_prop.PropertyType == typeof(int))
                    txtValue.TextChanged += txt_IntTextChanged;
                else if (entry_prop.PropertyType == typeof(double))
                    txtValue.TextChanged += txt_DoubleTextChanged;

                Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
                {
                    txtValue.Focus(); txtValue.CaretIndex = txtValue.Text.Length;
                }));
            }

            ctrl.Style = (Style)Application.Current.Resources["EntryControlStyle"];
            Grid.SetRow(ctrl, 1);
            Grid.SetColumn(ctrl, 0);
            entryGrid.Children.Add(ctrl);
        }

        public dlgEntry(Window owner, string prompt, List<Entity> options, Object obj, string prop_name)
        {
            InitCommon(owner, prompt);

            entry_obj = obj;
            entry_prop = obj.GetType().GetProperty(prop_name);
            cmbValue = new ComboBox();

            cmbValue.Background = Brushes.Beige;
            cmbValue.Style = (Style)Application.Current.Resources["AppComboStyle"];

            cmbValue.ItemsSource = options;
            cmbValue.DisplayMemberPath = "ID_NAME";
            cmbValue.SelectedValuePath = "ID";
            cmbValue.SelectedItem = options.FirstOrDefault(x => x.ID == (int)entry_prop.GetValue(entry_obj, null));

            Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(delegate ()
            {
                cmbValue.Focus();
            }));

            Grid.SetRow(cmbValue, 1);
            Grid.SetColumn(cmbValue, 0);
            entryGrid.Children.Add(cmbValue);

            cmbValue.SelectionChanged += CmbValue_SelectionChanged;
            cmbValue.KeyDown += CmbValue_KeyDown;
        }

        private void CmbValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void pwdValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                entry_prop.SetValue(entry_obj, pwdValue.Password, null);
                this.DialogResult = true;
            }
            else if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void txtValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    if (entry_prop.PropertyType == typeof(double))
                    {
                        entry_prop.SetValue(entry_obj, Double.Parse(txtValue.Text), null);
                    }
                    else if (entry_prop.PropertyType == typeof(int))
                    {
                        entry_prop.SetValue(entry_obj, Int32.Parse(txtValue.Text), null);
                    }
                    else
                    {
                        entry_prop.SetValue(entry_obj, txtValue.Text, null);
                    }

                    this.DialogResult = true;
                }
                catch
                {
                    this.DialogResult = false;
                }
            }
            else if (e.Key == Key.Escape)
            {
                this.DialogResult = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Owner.Effect = null;
            if (txtValue != null)
                TouchKeys.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Owner.Effect = new System.Windows.Media.Effects.BlurEffect();
            if (txtValue != null)
                TouchKeys.Open();
        }


        private void txt_IntTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            int val = -1;

            if ((textBox.Text!="-") && !Int32.TryParse(textBox.Text, out val))
            {
                TextChange textChange = e.Changes.ElementAt<TextChange>(0);
                int iAddedLength = textChange.AddedLength;
                int iOffset = textChange.Offset;
                textBox.Text = textBox.Text.Remove(iOffset, iAddedLength);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void txt_DoubleTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            double val = -1;

            if ((textBox.Text != "-") && !Double.TryParse(textBox.Text, out val))
            {
                TextChange textChange = e.Changes.ElementAt<TextChange>(0);
                int iAddedLength = textChange.AddedLength;
                int iOffset = textChange.Offset;
                textBox.Text = textBox.Text.Remove(iOffset, iAddedLength);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void lblPrompt_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
